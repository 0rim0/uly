'use strict';

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var array = createCommonjsModule(function (module) {
module.exports = {
	loop(arr) {
		return {
			pos: 0,
			get(force_index) {
				if (force_index || force_index === 0) {
					return this.arr[force_index]
				}
				const pos = this.pos;
				this.pos = ++this.pos % this.arr.length;
				return this.arr[pos]
			},
			reset() {
				this.pos = 0;
			},
			arr,
		}
	},

	wrap(arr) {
		return Array.isArray(arr) ? arr : [arr]
	},

	selectKV(index, item, key_sel, value_sel, excludes_key) {
		const k = typeof key_sel === "function" ? key_sel(item, index) : item[key_sel];
		let v = item;
		if (excludes_key) {
			const { [key_sel]: _, ...rem } = item;
			v = rem;
		} else if (value_sel) {
			v = typeof value_sel === "function" ? value_sel(item, index) : item[value_sel];
		}
		return [k, v]
	},

	toObject(arr, key, { excludes_key, value } = {}) {
		return Object.fromEntries(
			arr.map((e, i) => {
				return this.selectKV(i, e, key, value, excludes_key)
			})
		)
	},

	groupBy(arr, key, { excludes_key, value } = {}) {
		const result = {};
		for (const [index, item] of arr.entries()) {
			const [k, v] = this.selectKV(index, item, key, value, excludes_key);
			if (result[k]) {
				result[k].push(v);
			} else {
				result[k] = [v];
			}
		}
		return result
	},

	diff(arr1, arr2, selector1, selector2) {
		const s1 = selector1 == null ? x => x : typeof selector1 === "function" ? selector1 : x => x[selector1];
		const s2 = selector2 == null ? s1 : typeof selector2 === "function" ? selector2 : x => x[selector2];
		let rem = arr1.map(e => [e, s1(e)]);
		for (const aitem of arr2) {
			const ditem = s2(aitem);
			rem = rem.filter(item => ditem !== item[1]);
		}
		return rem.map(e => e[0])
	},

	intersect(arr1, arr2, selector1, selector2) {
		const s1 = selector1 == null ? x => x : typeof selector1 === "function" ? selector1 : x => x[selector1];
		const s2 = selector2 == null ? s1 : typeof selector2 === "function" ? selector2 : x => x[selector2];
		const result = [];
		const as2 = arr2.map(e => s2(e));
		for (const aitem of arr1) {
			const citem = s1(aitem);
			if (as2.includes(citem)) {
				result.push(aitem);
			}
		}
		return result
	},

	sort(arr, ...bys) {
		const formated_bys = bys.map(by => {
			if (typeof by === "string" || typeof by === "function") {
				return { by, order: "asc" }
			}
			if (Array.isArray(by)) {
				return { by: by[0], order: String(by[1] || "asc").toLowerCase() }
			}
			if (by) {
				return { by: by.by, order: String(by.order || "asc").toLowerCase() }
			}
			return { by: null, order: "asc" }
		});
		const order_sign = { asc: 1, desc: -1 };
		return arr.sort((x, y) => {
			for (const { by, order } of formated_bys) {
				const [xv, yv] = typeof by === "string" ? [x[by], y[by]] : typeof by === "function" ? [by(x), by(y)] : [x, y];
				if (xv < yv) return order_sign[order] * -1
				if (xv > yv) return order_sign[order] * 1
			}
			return 0
		})
	},

	find(arr, cb) {
		const index = arr.findIndex(cb);
		const found = index >= 0;
		const value = arr[index];
		return { index, found, value }
	},

	lastFind(arr, cb) {
		for (let i = arr.length - 1; i >= 0; i--) {
			if (cb(arr[i], i, arr)) {
				return { index: i, found: true, value: arr[i] }
			}
		}
		return { index: -1, found: false, value: undefined }
	},

	remove(arr, condition, all) {
		condition = condition || {};
		let removed_num = 0;
		for (let i = 0; i < arr.length; ) {
			if (removed_num === 1 && !all) break
			if ("value" in condition) {
				if (condition.value === arr[i]) {
					arr.splice(i, 1);
					removed_num++;
					continue
				}
			}
			if ("index" in condition) {
				if (Array.isArray(condition.index)) {
					if (condition.index.includes(i + removed_num)) {
						arr.splice(i, 1);
						removed_num++;
						continue
					}
				} else {
					if (condition.index === i + removed_num) {
						arr.splice(i, 1);
						removed_num++;
						continue
					}
				}
			}
			if ("match" in condition) {
				if (condition.match(arr[i], i + removed_num)) {
					arr.splice(i, 1);
					removed_num++;
					continue
				}
			}
			i++;
		}
		return arr
	},

	repeat(arr, num) {
		let result = [];
		for (let i = 0; i < ~~num; i++) result.push(...arr);
		return result
	},

	slices(arr, lengths) {
		let i = 0;
		const result = [];
		for (const length of lengths) {
			result.push(arr.slice(i, i + ~~length));
			i += ~~length;
		}
		return result
	},

	reader(list, max_repeat) {
		list = typeof list.next === "function" ? [...list] : list;
		max_repeat = +max_repeat || 0;
		let index = 0;
		let repeat = 0;

		return read_num => {
			if (list.length === 0) return items.slice(0, 0)
			const num = ~~read_num || 1;
			let result = list.slice(index, index + num);
			index += num;
			while (result.length < num) {
				if (++repeat > max_repeat) break
				index = 0;
				const shortage = num - result.length;
				const part = list.slice(index, index + shortage);
				result = result.concat(part);
				index += shortage;
			}
			return result
		}
	},

	get(arr, index) {
		if (!Array.isArray(index)) {
			let i = ~~index;
			if (i < 0) i = arr.length + i;
			return arr[i]
		}
		return index.map(i => {
			if (!Array.isArray(i)) {
				return module.exports.get(arr, i)
			} else {
				const [from, to] = i;
				return arr.slice(from, to)
			}
		})
	},
};
});
var array_1 = array.loop;
var array_2 = array.wrap;
var array_3 = array.selectKV;
var array_4 = array.toObject;
var array_5 = array.groupBy;
var array_6 = array.diff;
var array_7 = array.intersect;
var array_8 = array.sort;
var array_9 = array.find;
var array_10 = array.lastFind;
var array_11 = array.remove;
var array_12 = array.repeat;
var array_13 = array.slices;
var array_14 = array.reader;
var array_15 = array.get;

var date = createCommonjsModule(function (module) {
module.exports = {
	format(format, date) {
		return format.replace(/yyyy|MM|dd|HH|mm|ss|fff/g, e => {
			if (e === "yyyy") return date.getFullYear()
			if (e === "MM") return String(date.getMonth() + 1).padStart(2, "0")
			if (e === "dd") return String(date.getDate()).padStart(2, "0")
			if (e === "HH") return String(date.getHours()).padStart(2, "0")
			if (e === "mm") return String(date.getMinutes()).padStart(2, "0")
			if (e === "ss") return String(date.getSeconds()).padStart(2, "0")
			if (e === "fff") return String(date.getMilliseconds()).padStart(3, "0")
		})
	},

	add(date, { year, month, day, hour, minute, second, millisecond }) {
		return new Date(
			date.getFullYear() + (year || 0),
			date.getMonth() + (month || 0),
			date.getDate() + (day || 0),
			date.getHours() + (hour || 0),
			date.getMinutes() + (minute || 0),
			date.getSeconds() + (second || 0),
			date.getMilliseconds() + (millisecond || 0)
		)
	},

	modify(date, opt) {
		const current = {
			year: date.getFullYear(),
			month: date.getMonth(),
			day: date.getDate(),
			hour: date.getHours(),
			minute: date.getMinutes(),
			second: date.getSeconds(),
			millisecond: date.getMilliseconds(),
		};
		const resolve = type =>
			typeof opt["a" + type] === "number"
				? opt["a" + type]
				: typeof opt["r" + type] === "number"
					? current[type] + opt["r" + type]
					: current[type];
		return new Date(
			resolve("year"),
			resolve("month"),
			resolve("day"),
			resolve("hour"),
			resolve("minute"),
			resolve("second"),
			resolve("millisecond")
		)
	},

	trunc(date, unit) {
		const units = ["year", "month", "day", "hour", "minute", "second", "millisecond"];
		const index = units.indexOf(unit);
		const args = [
			date.getFullYear(),
			date.getMonth(),
			date.getDate(),
			date.getHours(),
			date.getMinutes(),
			date.getSeconds(),
			date.getMilliseconds(),
		].slice(0, index + 1);
		if (args.length === 1) args.push(0);
		return new Date(...args)
	},

	today() {
		return module.exports.trunc(new Date(), "day")
	},

	day_msec: 1000 * 3600 * 24,
	week_msec: 1000 * 3600 * 24 * 7,
};
});
var date_1 = date.format;
var date_2 = date.add;
var date_3 = date.modify;
var date_4 = date.trunc;
var date_5 = date.today;
var date_6 = date.day_msec;
var date_7 = date.week_msec;

var regexp = {
	escape(str) {
		return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
	},
};

var string = createCommonjsModule(function (module) {
module.exports = {
	replaceText(str, from, to) {
		return str.replace(new RegExp(regexp.escape(from), "g"), to)
	},

	splitChars(str, num) {
		const n = ~~num <= 0 ? Infinity : ~~num;
		let s = String(str);
		const result = [];
		while (s && result.length < n - 1) {
			const cp = s.codePointAt();
			const size = cp > 65535 ? 2 : 1;
			result.push(s.slice(0, size));
			s = s.slice(size);
		}
		if (s) result.push(s);
		return result
	},

	split(str, splitter, num) {
		const n = ~~num <= 0 ? Infinity : ~~num;
		let s = String(str);
		if (splitter instanceof RegExp) {
			if (splitter.test("")) return module.exports.splitChars(str, num)

			const result = [];
			let len = 0;
			while (s && len < n - 1) {
				const matched = s.match(splitter);
				if (!matched) {
					result.push(s);
					return result
				} else {
					result.push(s.slice(0, matched.index), ...matched.slice(1));
					s = s.slice(matched.index + matched[0].length);
					len++;
				}
			}
			result.push(s);
			return result
		} else {
			const sp = String(splitter);
			if (sp === "") return module.exports.splitChars(str, num)

			const result = [];
			while (s && result.length < n - 1) {
				const index = s.indexOf(sp);
				if (index < 0) {
					result.push(s);
					return result
				} else {
					result.push(s.slice(0, index));
					s = s.slice(index + sp.length);
				}
			}
			result.push(s);
			return result
		}
	},

	firstDiff(s1, s2) {
		if (s1 === s2) return -1
		let i = 0;
		while (s1[i] === s2[i]) i++;
		return i
	},

	fixedLeft(str, length, pad_chars = "0") {
		return String(str)
			.padEnd(length, pad_chars)
			.slice(0, length)
	},

	fixedRight(str, length, pad_chars = "0") {
		return String(str)
			.padStart(length, pad_chars)
			.slice(-length)
	},
};
});
var string_1 = string.replaceText;
var string_2 = string.splitChars;
var string_3 = string.split;
var string_4 = string.firstDiff;
var string_5 = string.fixedLeft;
var string_6 = string.fixedRight;

var dsv = {
	parse(text, { col_delimiter = ",", quote = '"', row_delimiter = "\n" } = {}) {
		const result = [];
		const row = [];
		let i = 0;
		while (true) {
			if (text.startsWith(quote, i)) {
				// quote case
				const si = i + quote.length;
				i = si;
				while (true) {
					const qi = text.indexOf(quote, i);
					if (qi === -1) {
						// invalid case
						row.push(text.slice(si));
						result.push(row.slice());
						return result
					}
					if (text.startsWith(quote + quote, qi)) {
						// escaped quote
						i = qi + quote.length * 2;
						continue
					}
					if (text.startsWith(quote + col_delimiter, qi)) {
						// found quote end and col delimiter
						row.push(string.replaceText(text.slice(si, qi), quote + quote, quote));
						i = qi + quote.length + col_delimiter.length;
						break
					}
					if (text.startsWith(quote + row_delimiter, qi)) {
						// found quote end and row delimiter
						row.push(string.replaceText(text.slice(si, qi), quote + quote, quote));
						result.push(row.slice());
						row.length = 0;
						i = qi + quote.length + row_delimiter.length;
						break
					}
					if (text.length === qi + quote.length) {
						// found quote and eof
						row.push(string.replaceText(text.slice(si, qi), quote + quote, quote));
						result.push(row.slice());
						return result
					}
					// found quote end but no delimiter
					i = qi + quote.length;
				}
			} else {
				// no quote case
				const cdi = text.indexOf(col_delimiter, i);
				const rdi = text.indexOf(row_delimiter, i);
				if (cdi === -1 && rdi === -1) {
					// standard end
					row.push(text.slice(i));
					result.push(row.slice());
					return result
				}
				if (cdi === -1 || (rdi > -1 && cdi > rdi)) {
					// found row delimiter
					row.push(text.slice(i, rdi));
					result.push(row.slice());
					row.length = 0;
					i = rdi + row_delimiter.length;
				} else {
					// found col delimiter
					row.push(text.slice(i, cdi));
					i = cdi + col_delimiter.length;
				}
			}
		}
	},

	build(aoa, { col_delimiter = ",", quote = '"', shouldQuote, row_delimiter = "\n" } = {}) {
		shouldQuote =
			shouldQuote || (text => text.includes(col_delimiter) || text.includes(row_delimiter) || text.startsWith(quote));
		return aoa
			.map(row =>
				row
					.map(cell => {
						let text = String(cell);
						return shouldQuote(text) ? quote + string.replaceText(text, quote, quote + quote) + quote : text
					})
					.join(col_delimiter)
			)
			.join(row_delimiter)
	},
};

var html = {
	escape(text) {
		const map = { "&": "&amp;", '"': "&quot;", "<": "&lt;", ">": "&gt;" };
		return text.replace(/["&<>]/g, e => map[e])
	},

	unescape(html) {
		const map = { "&amp;": "&", "&quot;": '"', "&lt;": "<", "&gt;": ">" };
		return html.replace(/&amp;|&quot;|&lt;|&gt;/g, e => map[e])
	},
};

var loop = n => new Loop(n);

class Loop {
	constructor(num) {
		this.num = ~~num;
	}

	for(fn) {
		for (let i = 0; i < this.num; i++) {
			if (fn(i, this.num) === true) return
		}
	}

	map(fn) {
		const result = [];
		for (let i = 0; i < this.num; i++) {
			result.push(fn(i, this.num));
		}
		return result
	}
}

var number = createCommonjsModule(function (module) {
module.exports = {
	sum(...nums) {
		return nums.reduce((a, e) => a + (+e || 0), 0)
	},

	average(...nums) {
		return module.exports.sum(...nums) / nums.length
	},

	median(...nums) {
		if (nums.length === 0) return NaN
		const sorted = nums.map(e => +e).sort();
		if (nums.length % 2) {
			return sorted[~~(nums.length / 2)]
		} else {
			return (sorted[nums.length / 2 - 1] + sorted[nums.length / 2]) / 2
		}
	},

	cap(num, min, max) {
		if (typeof min === "number") num = Math.max(min, num);
		if (typeof max === "number") num = Math.min(max, num);
		return num
	},
};
});
var number_1 = number.sum;
var number_2 = number.average;
var number_3 = number.median;
var number_4 = number.cap;

var object = {
	copy(value) {
		if (value instanceof Object) {
			return JSON.parse(JSON.stringify(value))
		}
		return value
	},

	pick(obj, keys) {
		const result = {};
		for (const k of keys) {
			if (k in obj) result[k] = obj[k];
		}
		return result
	},

	withhold(obj, keys) {
		const result = {};
		const key_set = new Set(keys);
		for (const [k, v] of Object.entries(obj)) {
			if (!key_set.has(k)) result[k] = v;
		}
		return result
	},

	filter(obj, fn) {
		const result = {};
		for (const [k, v] of Object.entries(obj)) {
			if (fn(v, k, obj)) result[k] = v;
		}
		return result
	},

	map(obj, fn) {
		const result = {};
		for (const [k, v] of Object.entries(obj)) {
			result[k] = fn(v, k, obj);
		}
		return result
	},

	flatMap(obj, fn) {
		const result = {};
		for (const [k, v] of Object.entries(obj)) {
			Object.assign(result, fn ? fn(v, k, obj) : v);
		}
		return result
	},

	diff(from, to, compare) {
		const added = new Set(Object.keys(to));
		const removed = new Set();
		const unchanged = new Set();
		const changed = new Set();
		for (const key of Object.keys(from)) {
			if (added.has(key)) {
				added.delete(key);
				const equal = typeof compare === "function" ? compare(from[key], to[key]) : from[key] === to[key];
				if (equal) {
					unchanged.add(key);
				} else {
					changed.add(key);
				}
			} else {
				removed.add(key);
			}
		}
		return { added, removed, unchanged, changed }
	},
};

var pager = class {
	constructor(items, per_page, initial_page) {
		this.items = items;
		this.per_page = ~~per_page <= 0 ? 10 : ~~per_page;
		this.jump(initial_page);
	}

	getCurrent() {
		return this.items.slice(this.getStartIndex(), this.getEndIndex())
	}

	getStartIndex() {
		return this.per_page * this.page
	}

	getEndIndex() {
		return this.per_page * (this.page + 1)
	}

	getLastPage() {
		return number.cap(Math.ceil(this.items.length / this.per_page) - 1, 0)
	}

	hasNext() {
		return this.page < this.getLastPage()
	}

	hasPrev() {
		return this.page > 0
	}

	next() {
		if (this.hasNext()) this.page++;
		return this.page
	}

	prev() {
		if (this.hasPrev()) this.page--;
		return this.page
	}

	jump(page) {
		this.page = number.cap(~~page, 0, this.getLastPage());
	}
};

var promise = {
	create() {
		let resolve, reject;
		const promise = new Promise((r1, r2) => {
			resolve = r1;
			reject = r2;
		});
		return Object.assign(promise, { resolve, reject })
	},

	wait(ms) {
		return new Promise(r => setTimeout(r, ~~ms))
	},
};

var random = createCommonjsModule(function (module) {
module.exports = {
	frand(from, to) {
		if (to == null) {
			to = from;
			from = 0;
		}
		return Math.random() * (to - from) + from
	},

	irand(from, to) {
		return ~~module.exports.frand(from, to)
	},

	brand() {
		return Math.random() < 0.5
	},

	srand(num) {
		const sign = module.exports.irand(2) * 2 - 1;
		return sign * num || sign
	},

	str(length, base = 36) {
		let str = "";
		length = ~~length;
		while (str.length < length)
			str += Math.random()
				.toString(base)
				.slice(2);
		return str.slice(0, length)
	},

	strWithTime(length, opt) {
		opt = opt || {};
		const time_first = opt.time_pos === "before";
		const joiner = opt.joiner == null ? "_" : opt.joiner;
		const base = ~~opt.base || 36;
		const time = Date.now().toString(base);
		const rand = module.exports.str(length);
		return (time_first ? [time, rand] : [rand, time]).join(joiner)
	},

	pick(arr, count, can_duplicate) {
		if (count > 0) {
			if (can_duplicate) {
				return loop(count).map(module.exports.pick.bind(null, arr))
			} else {
				const copy = arr.slice();
				const result = [];
				loop(Math.min(count, copy.length)).for(() => {
					const idx = ~~(Math.random() * copy.length);
					result.push(copy[idx]);
					const t = copy.pop();
					if (idx < copy.length) copy[idx] = t;
				});
				return result
			}
		} else {
			return arr[~~(Math.random() * arr.length)]
		}
	},
};
});
var random_1 = random.frand;
var random_2 = random.irand;
var random_3 = random.brand;
var random_4 = random.srand;
var random_5 = random.str;
var random_6 = random.strWithTime;
var random_7 = random.pick;

var lib = {
	array: array,
	date: date,
	dsv: dsv,
	html: html,
	loop: loop,
	number: number,
	object: object,
	pager: pager,
	promise: promise,
	random: random,
	regexp: regexp,
	string: string,
};

var file = createCommonjsModule(function (module) {
module.exports = {
	/// a タグを使ってファイルをダウンロード
	downloadURL(url, name) {
		const a = document.createElement("a");
		a.href = url;
		a.download = name;
		a.click();
	},

	/// arraybuffer, blob, string, 他 json 化可能な値をダウンロード
	download(value, name) {
		const blob = (() => {
			// arraybuffer
			{
				let buffer;
				if (value instanceof ArrayBuffer) {
					buffer = value;
				} else if (value instanceof Object && value.buffer instanceof ArrayBuffer) {
					buffer = value.buffer;
				}

				if (buffer) return new Blob([buffer], { type: "application/octet-stream" })
			}
			// Blob
			if (value instanceof Blob) return value
			// string and other
			{
				const str = typeof value === "string" ? value : JSON.stringify(value, null, "\t");
				return new Blob([str], { type: "text/plain" })
			}
		})();

		const url = URL.createObjectURL(blob);
		module.exports.downloadURL(url, name);

		// メモリ解放
		// ブラウザローカルからのダウンロードで 1 分超えることはまずないと思うのでとりあえず 1 分
		setTimeout(() => URL.revokeObjectURL(url), 1000 * 60);
	},

	async openFileDialog(multiple, accept) {
		return new Promise(resolve => {
			const input = document.createElement("input");
			input.type = "file";
			if (multiple) input.multiple = true;
			if (accept) input.accept = accept;
			input.addEventListener("change", eve => {
				const files = eve.target.files;
				if (multiple) {
					resolve([...files]);
				} else {
					resolve(files[0] || null);
				}
			});
			input.click();
		})
	},

	async readFile(file, as) {
		return new Promise((resolve, reject) => {
			const fr = new FileReader();
			fr.onload = async eve => {
				resolve(fr.result);
			};
			fr.onerror = eve => {
				reject(fr.error);
			};
			const methods = {
				arraybuffer: "readAsArrayBuffer",
				dataurl: "readAsDataURL",
				text: "readAsText",
			};
			fr[methods[as]](file);
		})
	},
};
});
var file_1 = file.downloadURL;
var file_2 = file.download;
var file_3 = file.openFileDialog;
var file_4 = file.readFile;

var require$$1 = {
	has(key) {
		return key in localStorage
	},
	get(key, default_value) {
		const json = localStorage[key];
		return json ? JSON.parse(json) : default_value
	},
	set(key, value) {
		if (value === undefined) {
			this.remove(key);
			return
		}
		localStorage[key] = JSON.stringify(value);
	},
	remove(key) {
		localStorage.removeItem(key);
	},
};

var browser = {
	file: file,
	storage: require$$1,
};

var browser$1 = Object.assign({}, lib, browser);

module.exports = browser$1;
