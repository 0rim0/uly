import commonjs from "rollup-plugin-commonjs"

export default [
	{
		input: "index.js",
		output: {
			file: "index.bundle.js",
			format: "cjs",
		},
		plugins: [commonjs()],
	},
	{
		input: "browser.js",
		output: {
			file: "browser.bundle.js",
			format: "cjs",
		},
		plugins: [commonjs()],
	},
	{
		input: "node.js",
		output: {
			file: "node.bundle.js",
			format: "cjs",
		},
		plugins: [commonjs()],
	},
]
