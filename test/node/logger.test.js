const logger = require("../../lib/node/logger.js")

test("write", async () => {
	let written = null
	const log = logger({
		level: "debug",
		stringify(values, level) {
			return `[${level}] ${values.join("\n")}`
		},
		format(text, level) {
			return level + ":" + text
		},
		write(text) {
			written = text
		},
	})

	await log.info("info")
	expect(written).toBe("info:[info] info")

	await log.info("aaa", 123)
	expect(written).toBe("info:[info] aaa\n123")

	await log.info(new Error("ERROR"))
	expect(written).toBe("info:[info] Error: ERROR")

	await log.warn("warn")
	expect(written).toBe("warn:[warn] warn")

	await log.error("error")
	expect(written).toBe("error:[error] error")

	await log.debug("debug")
	expect(written).toBe("debug:[debug] debug")
})

test("write level", async () => {
	let written = null
	const log = logger({
		level: "warn",
		write(text) {
			written = "written"
		},
	})

	written = null
	await log.debug()
	expect(written).toBe(null)

	written = null
	await log.info()
	expect(written).toBe(null)

	written = null
	await log.warn()
	expect(written).toBe("written")

	written = null
	await log.error()
	expect(written).toBe("written")
})

test("console", async () => {
	const log = logger({
		write() {},
		console: true,
	})

	const c = window.console
	let text = ""
	window.console = {
		info(...a) {
			text += "info " + a.join(" ") + "\n"
		},
		warn(...a) {
			text += "warn " + a.join(" ") + "\n"
		},
		error(...a) {
			text += "error " + a.join(" ") + "\n"
		},
	}

	await log.info("abc")
	await log.warn("a", "b")
	await log.error(100)

	window.console = c
	expect(text).toBe("info abc\nwarn a b\nerror 100\n")
})
