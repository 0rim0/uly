const multiLogger = require("../../lib/node/multi-logger.js")

test("log", async () => {
	const log1 = []
	const log2 = []
	const log3 = []
	const logger = multiLogger([
		(level, values) => log1.push({ level, values }),
		(level, values) => log2.push({ level, values }),
		{
			min_level: "info",
			write(level, values) {
				log3.push({ level, values })
			},
		},
	])
	logger.info("a", 1)
	logger.warn("b", "c", "d")
	logger.error(false, null)
	logger.debug(10, 20)

	expect(log1).toEqual([
		{ level: "info", values: ["a", 1] },
		{ level: "warn", values: ["b", "c", "d"] },
		{ level: "error", values: [false, null] },
		{ level: "debug", values: [10, 20] },
	])

	expect(log2).toEqual([
		{ level: "info", values: ["a", 1] },
		{ level: "warn", values: ["b", "c", "d"] },
		{ level: "error", values: [false, null] },
		{ level: "debug", values: [10, 20] },
	])

	expect(log3).toEqual([
		{ level: "info", values: ["a", 1] },
		{ level: "warn", values: ["b", "c", "d"] },
		{ level: "error", values: [false, null] },
	])
})
