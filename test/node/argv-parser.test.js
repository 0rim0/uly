const ap = require("../../lib/node/argv-parser.js")

test("basic", () => {
	let result
	result = ap.parse(
		{
			params: {
				foo: String,
				f: "foo",
				bar: Boolean,
				baz: Number,
			},
		},
		["-f", "12", "--bar", "a", "--baz", "23", "--extra", "b"]
	)
	expect(result).toEqual({ params: { foo: "12", bar: true, baz: 23 }, args: ["a", "b"], remains: [] })

	result = ap.parse(
		{
			params: {
				str: String,
			},
			extra_params: Number,
		},
		["--aa", "12", "--b", "23", "--cc", "--str"]
	)
	expect(result).toEqual({ params: { aa: 12, b: 23, str: "" }, args: [], remains: [] })
})

test("default", () => {
	let result
	result = ap.parse(
		{
			params: {
				a: Number,
				b: { type: Number, default: 1 },
				c: { type: Number, default: 1 },
				d: { type: Number, default: 0 },
			},
		},
		["--b", "0"]
	)
	expect(result).toEqual({ params: { a: 0, b: 0, c: 1, d: 0 }, args: [], remains: [] })
})

test("stop", () => {
	let result
	result = ap.parse(
		{
			params: {
				aaa: String,
				bbb: String,
				ccc: String,
			},
			stop: "!",
		},
		["--aaa", "123", "!", "--bbb", "1", "-c", "2"]
	)
	expect(result).toEqual({
		params: { aaa: "123", bbb: "", ccc: "" },
		args: [],
		remains: ["!", "--bbb", "1", "-c", "2"],
	})

	result = ap.parse(
		{
			params: {
				aaa: String,
				bbb: String,
				ccc: String,
			},
			stop: "!",
		},
		["--aaa", "!", "--bbb", "1", "-c", "2"]
	)
	expect(result).toEqual({
		params: { aaa: "!", bbb: "1", ccc: "" },
		args: ["2"],
		remains: [],
	})

	result = ap.parse(
		{
			params: {
				aaa: String,
				bbb: String,
				ccc: String,
			},
			force_stop: /!/,
		},
		["--aaa", "[!]", "--bbb", "1", "-c", "2"]
	)
	expect(result).toEqual({
		params: { aaa: "", bbb: "", ccc: "" },
		args: [],
		remains: ["[!]", "--bbb", "1", "-c", "2"],
	})
})

test("alias", () => {
	let result
	result = ap.parse(
		{
			params: {
				abc: String,
				bcd: "abc",
				cde: "bcd",
				def: "cde",
			},
		},
		["--abc", "a", "--bcd", "b", "--cde", "c", "--def", "d"]
	)
	expect(result).toEqual({ params: { abc: "d" }, args: [], remains: [] })
})

test("short", () => {
	let result
	result = ap.parse(
		{
			params: {
				x: String,
				y: Boolean,
				z: Boolean,
			},
		},
		["-yzx", "1"]
	)
	expect(result).toEqual({ params: { y: true, z: true, x: "1" }, args: [], remains: [] })

	result = ap.parse(
		{
			params: {
				x: String,
				y: Boolean,
				z: Boolean,
			},
		},
		["-zxya", "1"]
	)
	expect(result).toEqual({ params: { y: false, z: true, x: "ya" }, args: ["1"], remains: [] })

	result = ap.parse(
		{
			params: {
				x: String,
				y: Boolean,
				z: Boolean,
			},
		},
		["-x=ab", "-yz=bc"]
	)
	expect(result).toEqual({ params: { y: true, z: true, x: "ab" }, args: [], remains: [] })
})

test("raw", () => {
	result = ap.parse(
		{
			params: {
				xx: String,
				yy: Boolean,
				zz: Number,
			},
			force_stop: "--",
			raw: true,
		},
		["a", "--zz", "1", "-x=1", "--x", "ab", "-y", "--yy", "c", "--xx", "--", "2"]
	)
	expect(result).toEqual([
		{ key: null, value: "a" },
		{ key: "zz", value: "1" },
		{ key: "x", value: "1" },
		{ key: "x", value: null },
		{ key: null, value: "ab" },
		{ key: "y", value: null },
		{ key: "yy", value: null },
		{ key: null, value: "c" },
		{ key: "xx", value: null },
		{ remains: ["--", "2"] },
	])
})
