const BufferedStreamReader = require("../../lib/node/buffered-stream-reader.js")
const fs = require("fs")

test("read", async () => {
	const bsr = new BufferedStreamReader()
	const rs = fs.createReadStream(__filename)

	rs.on("data", bsr.write)
	rs.on("end", bsr.end)

	const buf = await bsr.read()
	const expect_buf = fs.readFileSync(__filename)
	expect(buf).toEqual(expect_buf)

	const str = await bsr.read("utf-8")
	expect(str).toBe(expect_buf.toString("utf-8"))
})

test("read pipe", async () => {
	const bsr = new BufferedStreamReader()
	const rs = fs.createReadStream(__filename)

	bsr.pipeFrom(rs)

	const buf = await bsr.read()
	const expect_buf = fs.readFileSync(__filename)
	expect(buf).toEqual(expect_buf)

	const str = await bsr.read("utf-8")
	expect(str).toBe(expect_buf.toString("utf-8"))
})
