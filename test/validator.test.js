const { validator } = require("../lib/validator.js")

test("typeof", () => {
	{
		const { valid, result } = validator.typeof("number")(1)
		expect(valid).toBe(true)
		expect(result).toBe(1)
	}
	{
		const { valid } = validator.typeof("string")(1)
		expect(valid).toBe(false)
	}
	{
		const { valid } = validator.typeof("null")(null)
		expect(valid).toBe(true)
	}
})

test("instanceof", () => {
	{
		const value = /a/
		const { valid, result } = validator.instanceof(RegExp)(value)
		expect(valid).toBe(true)
		expect(result).toBe(value)
	}
	{
		const { valid } = validator.typeof(Date)(/a/)
		expect(valid).toBe(false)
	}
})

test("and", () => {
	const Array2 = class extends Array {}
	const a = new Array2()
	const validate = validator.and(validator.typeof("array"), validator.instanceof(Array2))
	{
		const { valid, result } = validate(a)
		expect(valid).toBe(true)
		expect(result).toBe(a)
	}
	{
		const { valid } = validate([])
		expect(valid).toBe(false)
	}
	{
		const { valid } = validate({})
		expect(valid).toBe(false)
	}
})

test("or", () => {
	const validate = validator.or(validator.typeof("null"), validator.typeof("string"))
	{
		const { valid, result } = validate("a")
		expect(valid).toBe(true)
		expect(result).toBe("a")
	}
	{
		const { valid, result } = validate(null)
		expect(valid).toBe(true)
		expect(result).toBe(null)
	}
	{
		const { valid } = validate(undefined)
		expect(valid).toBe(false)
	}
})

test("is", () => {
	expect(validator.is(1, 2)(1).valid).toBe(true)
	expect(validator.is(1, 2)("1").valid).toBe(false)
	expect(validator.is(1, 2)(2).valid).toBe(true)
	expect(validator.is(1, 2)(null).valid).toBe(false)
})

test("not", () => {
	expect(validator.not(validator.is(1, 2))(1).valid).toBe(false)
	expect(validator.not(validator.is(1, 2))("1").valid).toBe(true)
	expect(validator.not(validator.is(1, 2))(2).valid).toBe(false)
	expect(validator.not(validator.is(1, 2))(null).valid).toBe(true)
})

test("range", () => {
	expect(validator.range({ min: 1, max: 3 })(1).valid).toBe(true)
	expect(validator.range({ min: 1, max: 3 })(-1).valid).toBe(false)
	expect(validator.range({ max: 3 })(-1).valid).toBe(true)
	expect(validator.range({ min: 1, exclude_min: true })(1).valid).toBe(false)
})

test("length", () => {
	expect(validator.length({ min: 1, max: 3 })("abc").valid).toBe(true)
	expect(validator.length({ min: 1, max: 3 })([1, 2]).valid).toBe(true)
	expect(validator.length({ min: 1 })("a").valid).toBe(true)
	expect(validator.length({ min: 1, exclude_min: true })("a").valid).toBe(false)
})

test("arrayof", () => {
	expect(validator.arrayof(validator.is(1, 2))([1, 2, 2, 1]).valid).toBe(true)
	expect(validator.arrayof(validator.is(1, 2))([1, 2, 2, 3]).valid).toBe(false)
	expect(validator.arrayof([validator.is(1), validator.is(2), validator.is(3)])([1, 2, 3]).valid).toBe(true)
	expect(validator.arrayof([validator.is(1), validator.is(2), validator.is(3)])([2, 2, 2]).valid).toBe(false)
})

test("property", () => {
	expect(validator.property({ a: validator.is(1), b: validator.is(2) })({ a: 1, b: 2 }).valid).toEqual(true)
	expect(validator.property({ a: validator.is(1), b: validator.is(2) })({ a: 1 }).valid).toEqual(false)
	expect(validator.property({ a: validator.is(1), b: validator.is(2) })({ a: 1, b: 2, c: 3 }).valid).toEqual(false)
})

test("unique", () => {
	expect(validator.unique()([1, 2, 3]).valid).toBe(true)
	expect(validator.unique()([1, 2, 3, 2]).valid).toBe(false)

	const { valid, result } = validator.unique({ convert: true })([1, 2, 3, 2])
	expect(valid).toBe(true)
	expect(result).toEqual([1, 2, 3])
})

test("int32", () => {
	expect(validator.int32()(1).valid).toBe(true)
	expect(validator.int32()(-2.3).valid).toBe(false)
	expect(validator.int32()("0").valid).toBe(false)

	{
		const { valid, result } = validator.int32({ convert: true })(1.23)
		expect(valid).toBe(true)
		expect(result).toEqual(1)
	}
	{
		const { valid, result } = validator.int32({ convert: true })("-2.3")
		expect(valid).toBe(true)
		expect(result).toEqual(-2)
	}
	{
		const { valid, result } = validator.int32({ convert: true })("a")
		expect(valid).toBe(false)
	}
	{
		const { valid, result } = validator.int32({ convert: true })("")
		expect(valid).toBe(false)
	}
	{
		const { valid, result } = validator.int32({ convert: "force" })("a")
		expect(valid).toBe(true)
		expect(result).toEqual(0)
	}
})

test("int", () => {
	expect(validator.int()(1).valid).toBe(true)
	expect(validator.int()(-2.3).valid).toBe(false)
	expect(validator.int()("0").valid).toBe(false)
	expect(validator.int()(Number.MAX_SAFE_INTEGER + 1).valid).toBe(false)

	{
		const { valid, result } = validator.int({ convert: true })(1.23)
		expect(valid).toBe(true)
		expect(result).toEqual(1)
	}
	{
		const { valid, result } = validator.int({ convert: true })("-2.3")
		expect(valid).toBe(true)
		expect(result).toEqual(-2)
	}
	{
		const { valid, result } = validator.int({ convert: true })(Infinity)
		expect(valid).toBe(true)
		expect(result).toEqual(Number.MAX_SAFE_INTEGER)
	}
	{
		const { valid, result } = validator.int({ convert: true })("a")
		expect(valid).toBe(false)
	}
	{
		const { valid, result } = validator.int({ convert: true })("")
		expect(valid).toBe(false)
	}
	{
		const { valid, result } = validator.int({ convert: "force" })("a")
		expect(valid).toBe(true)
		expect(result).toEqual(0)
	}
})

test("num", () => {
	expect(validator.num()(1).valid).toBe(true)
	expect(validator.num()(-2.3).valid).toBe(true)
	expect(validator.num()("0").valid).toBe(false)
	expect(validator.num()(Number.MAX_SAFE_INTEGER + 1).valid).toBe(true)

	{
		const { valid, result } = validator.num({ convert: true })("-2.3")
		expect(valid).toBe(true)
		expect(result).toEqual(-2.3)
	}
	{
		const { valid, result } = validator.num({ convert: true })("a")
		expect(valid).toBe(false)
	}
	{
		const { valid, result } = validator.num({ convert: true })("")
		expect(valid).toBe(false)
	}
	{
		const { valid, result } = validator.num({ convert: "force" })("a")
		expect(valid).toBe(true)
		expect(result).toEqual(0)
	}
})

test("nan", () => {
	expect(validator.nan({ is_nan: true })(NaN).valid).toBe(true)
	expect(validator.nan()(1).valid).toBe(true)
	expect(validator.nan()(NaN).valid).toBe(false)

	{
		const { valid, result } = validator.nan({ convert: true, alternative: 3 })(NaN)
		expect(valid).toBe(true)
		expect(result).toEqual(3)
	}
})

test("numstring", () => {
	expect(validator.numstring()("1").valid).toBe(true)
	expect(validator.numstring()("-2.3").valid).toBe(true)
	expect(validator.numstring()(1).valid).toBe(false)
	expect(validator.numstring()("a").valid).toBe(false)

	{
		const { valid, result } = validator.numstring({ convert: true })(1.2)
		expect(valid).toBe(true)
		expect(result).toEqual("1.2")
	}
	{
		const { valid, result } = validator.numstring({ convert: true })("a")
		expect(valid).toBe(false)
	}
	{
		const { valid, result } = validator.numstring({ convert: true })("")
		expect(valid).toBe(false)
	}
	{
		const { valid, result } = validator.numstring({ convert: "force" })("")
		expect(valid).toBe(true)
		expect(result).toEqual("0")
	}
})

test("url", () => {
	expect(validator.url()("https://xxx.yyy/").valid).toBe(true)
	expect(validator.url({ accept_scheme: "https" })("https://xxx.yyy/").valid).toBe(true)
	expect(validator.url({ accept_scheme: "http" })("https://xxx.yyy/").valid).toBe(false)

	{
		const { valid, result } = validator.url({ convert: true, base_url: "http://base/" })("/foo/bar")
		expect(valid).toBe(true)
		expect(result).toEqual("http://base/foo/bar")
	}
	{
		const url = "http://xxx.yyy/?z=あ"
		expect(validator.url()(url).valid).toBe(false)

		const { valid, result } = validator.url({ convert: true })(url)
		expect(valid).toBe(true)
		expect(result).toEqual(url.replace("あ", "%E3%81%82"))
	}
})

test("match", () => {
	expect(validator.match(/^\d+$/)("12324").valid).toBe(true)
	expect(validator.match(/^\d+$/)("12324a").valid).toBe(false)
})

test("when", () => {
	const validate = validator.when(
		[x => x.startsWith("A"), validator.length({ min: 2, max: 3 })],
		[x => x.startsWith("B"), validator.length({ min: 4, max: 5 })],
		validator.length({ max: 0 })
	)

	expect(validate("A1").valid).toBe(true)
	expect(validate("A123").valid).toBe(false)
	expect(validate("B12").valid).toBe(false)
	expect(validate("B1234").valid).toBe(true)
	expect(validate("C").valid).toBe(false)
	expect(validate("").valid).toBe(true)
})

test("cases", () => {
	const validate = validator.cases(x => x[0], {
		A: validator.length({ min: 2, max: 3 }),
		B: validator.length({ min: 4, max: 5 }),
	})

	expect(validate("A1").valid).toBe(true)
	expect(validate("A123").valid).toBe(false)
	expect(validate("B12").valid).toBe(false)
	expect(validate("B1234").valid).toBe(true)
	expect(validate("C").valid).toBe(false)
	expect(validate("").valid).toBe(false)
})

test("nullish", () => {
	expect(validator.nullish()("").valid).toBe(false)
	expect(validator.nullish()(false).valid).toBe(false)
	expect(validator.nullish()(null).valid).toBe(true)
	expect(validator.nullish()(undefined).valid).toBe(true)

	const sym = Symbol("NULL")
	expect(validator.nullish(sym)(sym).valid).toBe(true)
})

test("number", () => {
	expect(validator.number()(null).valid).toBe(false)
	expect(validator.number({ nullable: true })(null).valid).toBe(true)
	expect(validator.number({ min: 2, max: 3 })(3).valid).toBe(true)
	expect(validator.number({ min: 2, max: 3 })(4).valid).toBe(false)
})

test("string", () => {
	expect(validator.string()(null).valid).toBe(false)
	expect(validator.string({ nullable: true })(null).valid).toBe(true)
	expect(validator.string({ min: 2, max: 3 })("abc").valid).toBe(true)
	expect(validator.string({ min: 2, max: 3 })("abcd").valid).toBe(false)
})

test("boolean", () => {
	expect(validator.boolean()(null).valid).toBe(false)
	expect(validator.boolean({ nullable: true })(null).valid).toBe(true)
	expect(validator.boolean()(false).valid).toBe(true)
	expect(validator.boolean()(0).valid).toBe(false)
})

test("function", () => {
	expect(validator.function()(null).valid).toBe(false)
	expect(validator.function({ nullable: true })(null).valid).toBe(true)
	expect(validator.function()(() => {}).valid).toBe(true)
	expect(validator.function()(0).valid).toBe(false)
})

test("array", () => {
	expect(validator.array()(null).valid).toBe(false)
	expect(validator.array({ nullable: true })(null).valid).toBe(true)
	expect(validator.array({ length: 2 })([1, 2]).valid).toBe(true)
	expect(validator.array()(0).valid).toBe(false)
})

test("object", () => {
	expect(validator.object()(null).valid).toBe(false)
	expect(validator.object({ nullable: true })(null).valid).toBe(true)
	expect(validator.object()({ a: 1 }).valid).toBe(true)
	expect(validator.object()(0).valid).toBe(false)
})

test("date", () => {
	expect(validator.date()(null).valid).toBe(false)
	expect(validator.date({ nullable: true })(null).valid).toBe(true)
	expect(validator.date()(new Date()).valid).toBe(true)
	expect(validator.date()(0).valid).toBe(false)
	expect(validator.date()(new Date("!")).valid).toBe(false)
})
