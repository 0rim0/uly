const dsv = require("../lib/dsv.js")

test("build", () => {
	expect(dsv.build([[1, 2, 3], [4], [5, 6]])).toBe("1,2,3\n4\n5,6")
	expect(dsv.build([[1, 2], [3, 4]], { col_delimiter: "/", row_delimiter: ";" })).toBe("1/2;3/4")
	expect(dsv.build([["1,2", "a\nb"], ['"a"', "x"]])).toBe(`"1,2","a\nb"\n"""a""",x`)
	expect(dsv.build([["1,2", '"a"', "`1`"]], { quote: "`" })).toBe('`1,2`,"a",```1```')
	expect(dsv.build([[1, 2], [3, 4]], { shouldQuote: x => true })).toBe(`"1","2"\n"3","4"`)
})

test("parse", () => {
	expect(dsv.parse(`1,2\n3,4`)).toEqual([["1", "2"], ["3", "4"]])
	expect(dsv.parse(`1:2:3;4:5`, { col_delimiter: ":", row_delimiter: ";" })).toEqual([["1", "2", "3"], ["4", "5"]])
	expect(dsv.parse(`%a%,b,%c%%d%`, { quote: "%" })).toEqual([["a", "b", "c%d"]])
})
