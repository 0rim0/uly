const html = require("../lib/html.js")

test("escape&unescape 1", () => {
	const src = `<script type="module"></script>`
	const escaped = html.escape(src)
	expect(escaped).toBe(`&lt;script type=&quot;module&quot;&gt;&lt;/script&gt;`)
	expect(html.unescape(escaped)).toBe(src)
})

test("escape&unescape 2", () => {
	const src = `!"#$%&'()aあ1`
	const escaped = html.escape(src)
	expect(escaped).toBe(`!&quot;#$%&amp;'()aあ1`)
	expect(html.unescape(escaped)).toBe(src)
})
