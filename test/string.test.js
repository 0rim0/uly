const string = require("../lib/string.js")

test("replaceText", () => {
	expect(string.replaceText("A{* xxx *}A{* xxx *}A{* xxx *}A", "{* xxx *}", ".")).toBe("A.A.A.A")
})

test("splitChars", () => {
	expect(string.splitChars("abcd")).toEqual(["a", "b", "c", "d"])
	expect(string.splitChars("abcd", 3)).toEqual(["a", "b", "cd"])
	expect(string.splitChars("abcd", 1)).toEqual(["abcd"])
	expect(string.splitChars("💯🆗", 0)).toEqual(["💯", "🆗"])
})

test("split", () => {
	expect(string.split("a:b:c::d:e", ":", 2)).toEqual(["a", "b:c::d:e"])
	expect(string.split("a:b:c::d:e", ":", 0)).toEqual(["a", "b", "c", "", "d", "e"])
	expect(string.split("a{x}b{y}c{z}d", /\{(.)\}/)).toEqual(["a", "x", "b", "y", "c", "z", "d"])
	expect(string.split("a{x}b{y}c{z}d", /\{(.)\}/, 3)).toEqual(["a", "x", "b", "y", "c{z}d"])
})

test("firstDiff", () => {
	expect(string.firstDiff("abc", "aba")).toBe(2)
	expect(string.firstDiff("abc", "abcd")).toBe(3)
	expect(string.firstDiff("abc", "abc")).toBe(-1)
})

test("fixedLeft", () => {
	expect(string.fixedLeft("a", 6, "_#")).toBe("a_#_#_")
	expect(string.fixedLeft("abcd", 2, "_")).toBe("ab")
	expect(string.fixedLeft(12, 4, "0")).toBe("1200")
})

test("fixedRight", () => {
	expect(string.fixedRight("a", 6, "_#")).toBe("_#_#_a")
	expect(string.fixedRight("abcd", 2, "_")).toBe("cd")
	expect(string.fixedRight(12, 4, "0")).toBe("0012")
})
