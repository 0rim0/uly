const regexp = require("../lib/regexp.js")

test("escape", () => {
	expect(regexp.escape(`!"#$%&'()-=^~\\|[{@\`]}:*;+,<.>/?_`)).toBe(
		`!"#\\$%&'\\(\\)\\-=\\^~\\\\\\|\\[\\{@\`\\]\\}:\\*;\\+,<\\.>\\/\\?_`
	)
})
