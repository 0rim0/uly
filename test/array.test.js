const array = require("../lib/array.js")

test("loop", () => {
	const loop = array.loop([1, 2, 3])
	expect(loop.get()).toBe(1)
	expect(loop.get()).toBe(2)
	expect(loop.get()).toBe(3)
	expect(loop.get()).toBe(1)
	expect(loop.get(2)).toBe(3)
	loop.reset()
	expect(loop.get()).toBe(1)
})

test("wrap (not array)", () => {
	expect(array.wrap(1)).toEqual([1])
})

test("wrap (array)", () => {
	expect(array.wrap([1])).toEqual([1])
})

test("toObject", () => {
	expect(array.toObject([1, 2, 3], x => x)).toEqual({ 1: 1, 2: 2, 3: 3 })
	const val = [{ x: 1, y: 2 }, { x: 10, y: 20 }]
	expect(array.toObject(val, "x")).toEqual({ 1: { x: 1, y: 2 }, 10: { x: 10, y: 20 } })
	expect(array.toObject(val, "x", { excludes_key: true })).toEqual({ 1: { y: 2 }, 10: { y: 20 } })
	expect(
		array.toObject(val, "x", {
			value(item) {
				return item.y
			},
		})
	).toEqual({ 1: 2, 10: 20 })
})

test("groupBy", () => {
	expect(array.groupBy([1, 2, 3, 1], x => x)).toEqual({ 1: [1, 1], 2: [2], 3: [3] })
	const val = [{ x: 1, y: 2 }, { x: 1, y: 20 }]
	expect(array.groupBy(val, "x")).toEqual({ 1: [{ x: 1, y: 2 }, { x: 1, y: 20 }] })
	expect(array.groupBy(val, "y", { excludes_key: true })).toEqual({ 2: [{ x: 1 }], 20: [{ x: 1 }] })
	expect(
		array.groupBy(val, "x", {
			value(item) {
				return item.y
			},
		})
	).toEqual({ 1: [2, 20] })
})

test("diff", () => {
	expect(array.diff([1, 2, 3], [2, 4])).toEqual([1, 3])
	expect(array.diff([{ x: 1 }, { x: 2 }], [{ x: 1 }, { x: 3 }], "x")).toEqual([{ x: 2 }])
	expect(array.diff([{ x: 1 }, { x: 2 }], [{ y: 1 }, { y: 3 }], "x", "y")).toEqual([{ x: 2 }])
})

test("intersect", () => {
	expect(array.intersect([1, 2, 3], [2, 4])).toEqual([2])
	expect(array.intersect([{ x: 1 }, { x: 2 }], [{ x: 1 }, { x: 3 }], "x")).toEqual([{ x: 1 }])
	expect(array.intersect([{ x: 1 }, { x: 2 }], [{ y: 1 }, { y: 3 }], "x", "y")).toEqual([{ x: 1 }])
})

test("sort (single condition)", () => {
	expect(array.sort([3, 1, 4, 2, 9, 9, 2, -2], x => x)).toEqual([-2, 1, 2, 2, 3, 4, 9, 9])
	expect(array.sort([3, 1, 4, 2, 9, 9, 2, -2], x => 10 - x)).toEqual([9, 9, 4, 3, 2, 2, 1, -2])
	expect(array.sort([3, 1, 4, 2, 9, 9, 2, -2], [x => x, "ASC"])).toEqual([-2, 1, 2, 2, 3, 4, 9, 9])
	expect(array.sort([3, 1, 4, 2, 9, 9, 2, -2], { by: x => 10 - x, order: "Desc" })).toEqual([-2, 1, 2, 2, 3, 4, 9, 9])
})

test("sort (multiple condition)", () => {
	expect(
		array.sort(
			[
				{ x: 1, y: 2 },
				{ x: 2, y: 2 },
				{ x: 2, y: 1 },
				{ x: 1, y: 1 },
				{ x: 3, y: 2 },
				{ x: 2, y: 2 },
				{ x: 3, y: 1 },
				{ x: 3, y: 3 },
			],
			"x",
			"y"
		)
	).toEqual([
		{ x: 1, y: 1 },
		{ x: 1, y: 2 },
		{ x: 2, y: 1 },
		{ x: 2, y: 2 },
		{ x: 2, y: 2 },
		{ x: 3, y: 1 },
		{ x: 3, y: 2 },
		{ x: 3, y: 3 },
	])

	expect(
		array.sort(
			[
				{ x: 1, y: 2 },
				{ x: 2, y: 2 },
				{ x: 2, y: 1 },
				{ x: 1, y: 1 },
				{ x: 3, y: 2 },
				{ x: 2, y: 2 },
				{ x: 3, y: 1 },
				{ x: 3, y: 3 },
			],
			["y", "desc"],
			{
				by(v) {
					return v.x & 1
				},
				order: "asc",
			},
			{ by: "x", order: "desc" }
		)
	).toEqual([
		{ x: 3, y: 3 },
		{ x: 2, y: 2 },
		{ x: 2, y: 2 },
		{ x: 3, y: 2 },
		{ x: 1, y: 2 },
		{ x: 2, y: 1 },
		{ x: 3, y: 1 },
		{ x: 1, y: 1 },
	])
})

test("find", () => {
	expect(array.find([1, 2], x => x > 1)).toEqual({ found: true, index: 1, value: 2 })
	expect(array.find([{ x: true }, { x: false }], e => e.x)).toEqual({ found: true, index: 0, value: { x: true } })
	expect(array.find([], e => true)).toEqual({ found: false, index: -1, value: undefined })
	expect(array.find([false, false], e => e)).toEqual({ found: false, index: -1, value: undefined })
	expect(array.find([{ x: 10 }, { x: 20 }], e => e)).toEqual({ found: true, index: 0, value: { x: 10 } })
})

test("lastFind", () => {
	expect(array.lastFind([1, 2], x => x > 1)).toEqual({ found: true, index: 1, value: 2 })
	expect(array.lastFind([{ x: true }, { x: false }], e => e.x)).toEqual({ found: true, index: 0, value: { x: true } })
	expect(array.lastFind([], e => true)).toEqual({ found: false, index: -1, value: undefined })
	expect(array.lastFind([false, false], e => e)).toEqual({ found: false, index: -1, value: undefined })
	expect(array.lastFind([{ x: 10 }, { x: 20 }], e => e)).toEqual({ found: true, index: 1, value: { x: 20 } })
})

test("max", () => {
	expect(array.max([7, 2, 9, 1, 5])).toEqual({ indexes: [2], value: 9 })
	expect(array.max([7, 2, 7, 1, 5])).toEqual({ indexes: [0, 2], value: 7 })
})

test("min", () => {
	expect(array.min([3, 2, 4, 1, 5])).toEqual({ indexes: [3], value: 1 })
	expect(array.min([3, 2, 4, 2, 5])).toEqual({ indexes: [1, 3], value: 2 })
})

test("remove", () => {
	const a = [1, 2, 3, 1, 2]
	const r = array.remove(a, { value: 1 }, true)
	expect(r).toEqual([2, 3, 2])
	expect(r).toBe(a)

	expect(array.remove([1, 2, 1, 2], { value: 1 })).toEqual([2, 1, 2])
	expect(array.remove([1, 2, 3], { index: 1 })).toEqual([1, 3])
	expect(array.remove([0, 1, 2, 3, 4, 5, 6], { index: [1, 3, 4, 6] }, true)).toEqual([0, 2, 5])
	expect(array.remove([1, 2, 1, 2], { match: x => x > 1 })).toEqual([1, 1, 2])
	expect(array.remove([1, 2, 1, 2], { match: x => x > 1 }, true)).toEqual([1, 1])
	expect(
		array.remove(
			[0, 1, 2, 3, 4, 5, 6],
			{
				value: 2,
				index: 4,
				match(x, i) {
					return x === 5 || i === 6
				},
			},
			true
		)
	).toEqual([0, 1, 3])
})

test("repeat", () => {
	expect(array.repeat([1, 2], 3)).toEqual([1, 2, 1, 2, 1, 2])
	expect(array.repeat([1, 2], 0)).toEqual([])
	expect(array.repeat([], 3)).toEqual([])
})

test("slices", () => {
	expect(array.slices([...Array(10).keys()], [3, 1, 0, 2, 2])).toEqual([[0, 1, 2], [3], [], [4, 5], [6, 7]])
	expect(array.slices([1, 2], [3, 3, 4])).toEqual([[1, 2], [], []])
	expect(array.slices("ABabcd12345", [2, 4, 3, 3])).toEqual(["AB", "abcd", "123", "45"])
})

test("reader", () => {
	expect(array.reader([1, 2, 3], 0)(10)).toEqual([1, 2, 3])
	expect(array.reader([1, 2, 3], 1)(10)).toEqual([1, 2, 3, 1, 2, 3])
	expect(array.reader([1, 2, 3], 2)(10)).toEqual([1, 2, 3, 1, 2, 3, 1, 2, 3])
	expect(array.reader([1, 2, 3], Infinity)(10)).toEqual([1, 2, 3, 1, 2, 3, 1, 2, 3, 1])
	let read
	read = array.reader([1, 2, 3], 100)
	expect(read(2)).toEqual([1, 2])
	expect(read(2)).toEqual([3, 1])
	expect(read(2)).toEqual([2, 3])
	read = array.reader("abcd", 100)
	expect(read(3)).toEqual("abc")
	expect(read(3)).toEqual("dab")
	expect(read(3)).toEqual("cda")
	expect(read(1)).toEqual("b")
	expect(read(2)).toEqual("cd")
	read = array.reader(
		(function*() {
			for (let i = 100; i < 105; i++) yield i
		})(),
		100
	)
	expect(read(1)).toEqual([100])
	expect(read(2)).toEqual([101, 102])
	expect(read(4)).toEqual([103, 104, 100, 101])
})

test("get", () => {
	expect(array.get([1, 2, 3, 4, 5], 1)).toEqual(2)
	expect(array.get([1, 2, 3, 4, 5], -1)).toEqual(5)
	expect(array.get([1, 2, 3, 4, 5], [2, 4])).toEqual([3, 5])
	expect(array.get([1, 2, 3, 4, 5], [[0, 2], [1, -2], 1, [3]])).toEqual([[1, 2], [2, 3], 2, [4, 5]])
})

test("paste", () => {
	expect(array.paste([1, 2, 3], 0, [4, 5, 6])).toEqual([4, 5, 6])
	expect(array.paste([1, 2, 3], 1, [4, 5, 6])).toEqual([1, 4, 5])
	expect(array.paste([1, 2, 3], 2, [4, 5, 6], true)).toEqual([1, 2, 4, 5, 6])
})
