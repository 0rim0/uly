const date = require("../lib/date.js")

test("format", () => {
	expect(date.format("yyyy-MM-dd HH:mm:ss.fff", new Date(2000, 0, 1, 2, 3, 4, 5))).toBe("2000-01-01 02:03:04.005")
})

test("add", () => {
	const d = new Date(2020, 7, 20, 12, 20, 30)

	expect(date.add(d, { hour: 2 }) - d).toBe(1000 * 60 * 60 * 2)
	expect(date.add(d, { millisecond: 9999 }) - d).toBe(9999)
	expect(date.add(d, { day: -2, hour: 40, minute: -300, second: 900 }) - d).toBe(
		1000 * 900 + 1000 * 60 * -300 + 1000 * 60 * 60 * 40 + 1000 * 60 * 60 * 24 * -2
	)
})

test("modify", () => {
	const d = new Date("2019-09-01 12:30:45")

	expect(date.modify(d, { aday: 3, rhour: -2, rminute: 11, asecond: 33 })).toEqual(new Date("2019-09-03 10:41:33"))
	expect(
		date.modify(d, { ayear: 2030, amonth: 0, aday: 1, ahour: 9, aminute: 10, asecond: 20, amillisecond: 30 })
	).toEqual(new Date("2030-01-01 09:10:20.030"))
	expect(
		date.modify(d, { ryear: -1, rmonth: -1, rday: -1, rhour: -1, rminute: -1, rsecond: -1, rmillisecond: -1 })
	).toEqual(new Date("2018-07-31 11:29:43.999"))
})

test("trunc", () => {
	const d = new Date(2020, 7, 20, 12, 20, 30)

	expect(date.trunc(d, "year")).toEqual(new Date(2020, 0, 1))
	expect(date.trunc(d, "day")).toEqual(new Date(2020, 7, 20))
	expect(date.trunc(d, "minute")).toEqual(new Date(2020, 7, 20, 12, 20))
})

test("modifyDate", () => {
	// prettier-ignore
	expect(date.modifyDate(new Date("2020/10/10 13:30:10"), new Date("2020/08/20 23:11:30")))
		.toEqual(new Date("2020/08/20 13:30:10"))

	// prettier-ignore
	expect(date.modifyDate(new Date("2020/10/10 10:20:30.123"), new Date("2021/11/11")))
		.toEqual(new Date("2021/11/11 10:20:30.123"))
})

test("modifyTime", () => {
	// prettier-ignore
	expect(date.modifyTime(new Date("2020/10/10 13:30:10"), new Date("2020/08/20 23:11:30")))
		.toEqual(new Date("2020/10/10 23:11:30"))

	// prettier-ignore
	expect(date.modifyTime(new Date("2020/10/10 10:20:30.123"), new Date("2021/11/11")))
		.toEqual(new Date("2020/10/10 00:00:00.000"))
})

test("modifyNearestTime", () => {
	// prettier-ignore
	expect(date.modifyNearestTime(new Date("2020/11/12 23:10:20"), new Date("2000/01/01 22:30:40.234")))
		.toEqual(new Date("2020/11/12 22:30:40.234"))

	// prettier-ignore
	expect(date.modifyNearestTime(new Date("2020/11/12 23:10:20"), new Date("2000/01/01 02:11:22.333")))
		.toEqual(new Date("2020/11/13 02:11:22.333"))

	// prettier-ignore
	expect(date.modifyNearestTime(new Date("2020/11/12 03:10:20"), new Date("2000/01/01 22:30:40.234")))
		.toEqual(new Date("2020/11/11 22:30:40.234"))
})

test("fromTime", () => {
	expect(date.fromTime("20:30:40", new Date("2010/06/07 11:22:33"))).toEqual(new Date("2010/06/07 20:30:40"))
	expect(date.fromTime("08:22:50.876", new Date("2010/06/07 11:22:33"))).toEqual(new Date("2010/06/07 08:22:50.876"))

	expect(+date.fromTime("25:00:00", new Date("2010/06/07 11:22:33"))).toBeNaN()
})

test("today", () => {
	const now = new Date()

	expect(date.today().getFullYear()).toEqual(now.getFullYear())
	expect(date.today().getMonth()).toEqual(now.getMonth())
	expect(date.today().getDate()).toEqual(now.getDate())
})
