const loop = require("../lib/loop.js")

test("for", () => {
	let x = 0
	loop(10).for(v => (x += v))
	expect(x).toBe(45)
})

test("for (break)", () => {
	let x = 0
	loop(10).for(v => {
		x = v
		if (v === 3) return true
	})
	expect(x).toBe(3)
})

test("map", () => {
	expect(loop(5).map(x => x)).toEqual([0, 1, 2, 3, 4])
})
