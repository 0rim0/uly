const number = require("../lib/number.js")

test("sum", () => {
	expect(number.sum(1, 2, 3)).toBe(6)
	expect(number.sum("1", "20", "300")).toBe(321)
})

test("average", () => {
	expect(number.average(1, 2, 3)).toBe(2)
	expect(number.average("1", "20", "300")).toBe(107)
})

test("median", () => {
	expect(number.median(1, 2, 3)).toBe(2)
	expect(number.median("1", "20", "300")).toBe(20)
	expect(number.median(10, 200, 3000, 40000)).toBe(1600)
})

test("cap", () => {
	expect(number.cap(10, 1, 20)).toBe(10)
	expect(number.cap(22, 1, 20)).toBe(20)
	expect(number.cap(0.3, 1, 20)).toBe(1)
	expect(number.cap(1000, 10)).toBe(1000)
	expect(number.cap(-1000, null, -10)).toBe(-1000)
})

test("about", () => {
	expect(number.about(10000, 8999, 10)).toBe(false)
	expect(number.about(10000, 9000, 10)).toBe(true)
	expect(number.about(10000, 11000, 10)).toBe(true)
	expect(number.about(10000, 11001, 10)).toBe(false)

	expect(number.about(10000, 8999, { percent: 10 })).toBe(false)
	expect(number.about(10000, 9000, { percent: 10 })).toBe(true)
	expect(number.about(10000, 11000, { percent: 10 })).toBe(true)
	expect(number.about(10000, 11001, { percent: 10 })).toBe(false)

	expect(number.about(10000, 9989, { diff: 10 })).toBe(false)
	expect(number.about(10000, 9990, { diff: 10 })).toBe(true)
	expect(number.about(10000, 10010, { diff: 10 })).toBe(true)
	expect(number.about(10000, 10011, { diff: 10 })).toBe(false)

	expect(number.about(10000, 4999, { min_percent: 50, max_percent: 120 })).toBe(false)
	expect(number.about(10000, 5000, { min_percent: 50, max_percent: 120 })).toBe(true)
	expect(number.about(10000, 12000, { min_percent: 50, max_percent: 120 })).toBe(true)
	expect(number.about(10000, 12001, { min_percent: 50, max_percent: 120 })).toBe(false)

	expect(number.about(10000, 9994, { min_diff: 5, max_diff: 13 })).toBe(false)
	expect(number.about(10000, 9995, { min_diff: 5, max_diff: 13 })).toBe(true)
	expect(number.about(10000, 10013, { min_diff: 5, max_diff: 13 })).toBe(true)
	expect(number.about(10000, 10014, { min_diff: 5, max_diff: 13 })).toBe(false)

	expect(number.about(10000, 9875, { min_num: 9876, max_num: 12345 })).toBe(false)
	expect(number.about(10000, 9876, { min_num: 9876, max_num: 12345 })).toBe(true)
	expect(number.about(10000, 12345, { min_num: 9876, max_num: 12345 })).toBe(true)
	expect(number.about(10000, 12346, { min_num: 9876, max_num: 12345 })).toBe(false)
})
