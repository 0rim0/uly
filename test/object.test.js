const object = require("../lib/object.js")

test("copy", () => {
	{
		const value = { a: 10, b: [2, 3] }
		const copy = object.copy(value)
		value.a = 100
		value.b.unshift(10)
		expect(copy).toEqual({ a: 10, b: [2, 3] })
	}
	{
		const value = { a: 10, b: [2, 3] }
		const copy = object.copy(value)
		copy.a = 100
		copy.b.unshift(10)
		expect(value).toEqual({ a: 10, b: [2, 3] })
	}
})

test("pick", () => {
	expect(object.pick({ a: 1, b: 2, c: 3 }, ["a", "c"])).toEqual({ a: 1, c: 3 })
})

test("withhold", () => {
	expect(object.withhold({ a: 1, b: 2, c: 3 }, ["a", "c"])).toEqual({ b: 2 })
})

test("filter", () => {
	expect(object.filter({ a: 1, b: 2, c: 3 }, (v, k) => v > 2)).toEqual({ c: 3 })
})

test("map", () => {
	expect(object.map({ a: { x: 10 }, b: { x: 20 }, c: { x: 30 } }, (v, k) => v.x / 10)).toEqual({ a: 1, b: 2, c: 3 })
})

test("flatMap", () => {
	expect(
		object.flatMap({
			x: { xa: 1, xb: 2 },
			y: { ya: 3, yb: 4 },
		})
	).toEqual({ xa: 1, xb: 2, ya: 3, yb: 4 })

	expect(
		object.flatMap(
			{
				a: false,
				b: true,
			},
			(v, k) => (v ? { [k]: 1, [k + k]: 2 } : {})
		)
	).toEqual({ b: 1, bb: 2 })
})

test("diff", () => {
	expect(object.diff({ a: 1, b: 2, c: 3 }, { b: 10, c: 3, d: 4 })).toEqual({
		added: new Set(["d"]),
		removed: new Set(["a"]),
		unchanged: new Set(["c"]),
		changed: new Set(["b"]),
	})

	expect(
		object.diff(
			{ x: { value: 0 }, y: { value: 1 } },
			{ x: { value: 0 }, y: { value: 0 } },
			(a, b) => a.value === b.value
		)
	).toEqual({
		added: new Set(),
		removed: new Set(),
		unchanged: new Set(["x"]),
		changed: new Set(["y"]),
	})

	const a = [1, 2, 3]
	const b = [2, 4, 1]
	const toObj = x => Object.fromEntries(x.map(e => [e, true]))
	expect(object.diff(toObj(a), toObj(b))).toEqual({
		added: new Set(["4"]),
		removed: new Set(["3"]),
		unchanged: new Set(["1", "2"]),
		changed: new Set(),
	})
})
