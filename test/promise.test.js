const promise = require("../lib/promise.js")

test("create", async () => {
	{
		const p = promise.create()
		expect(p).toBeInstanceOf(Promise)
		p.resolve("A")
		expect(p).resolves.toBe("A")
	}
	{
		const p = promise.create()
		p.reject("B")
		expect(p).rejects.toBe("B")
	}
})

test("wait", async () => {
	const t = Date.now()
	await promise.wait(1000)
	const diff = Date.now() - t
	expect(diff > 1000 * 0.9 && diff < 1000 * 1.1).toBe(true)
})

test("resolve", async () => {
	expect(promise.resolve(Promise.resolve(10))).resolves.toEqual({ ok: true, result: 10 })
	expect(promise.resolve(Promise.reject(20))).resolves.toEqual({ ok: false, error: 20 })
})
