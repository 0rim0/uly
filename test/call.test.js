const call = require("../lib/call.js")
const { about } = require("../lib/number.js")

test("createWhen", () => {
	let value

	value = call.createWhen(() => 1, () => 2)(true)
	expect(value).toBe(1)

	value = call.createWhen(() => 1, () => 2)(false)
	expect(value).toBe(2)

	value = call.createWhen(a => a + 1, a => a + 2)(100)
	expect(value).toBe(101)

	value = call.createWhen(a => a + 1, a => a + 2)(0)
	expect(value).toBe(2)

	value = call.createWhen(10, 20)(1)
	expect(value).toBe(10)

	value = call.createWhen(10, 20)(0)
	expect(value).toBe(20)

	value = call.createWhen((x, y) => y, (x, y) => -y)(false, 10)
	expect(value).toBe(-10)
})

test("when", () => {
	value = call.when(true, 10, 20)
	expect(value).toBe(10)

	value = call.when(false, 10, 20)
	expect(value).toBe(20)
})

test("createCase", () => {
	let value

	value = call.createCase({
		x() {
			return 1
		},
		y() {
			return 2
		},
	})("x")
	expect(value).toBe(1)

	value = call.createCase({
		x() {
			return 1
		},
		y() {
			return 2
		},
	})("y")
	expect(value).toBe(2)

	value = call.createCase({
		x() {
			return 1
		},
		y() {
			return 2
		},
		default() {
			return 3
		},
	})("z")
	expect(value).toBe(3)

	value = call.createCase(new Map([[1, () => 10], [2, () => 20], [null, () => 30]]))(10)
	expect(value).toBe(30)

	{
		const obj1 = { a: 1 }
		const obj2 = { b: 2 }
		const map = new Map([[obj1, x => x.a + 10], [obj2, x => x.b + 20]])
		value = call.createCase(map)(obj2)
		expect(value).toBe(22)
	}

	value = call.createCase({ foo: (x, y) => x + y })("foo", "bar")
	expect(value).toBe("foobar")
})

test("case", () => {
	expect(call.case("a", { a: () => 999 })).toBe(999)
})

test("do", async () => {
	expect(call.do(() => 1)).toBe(1)
	expect(call.do(() => 1, x => x + 1, x => x + 2)).toBe(4)
	expect(call.do(1, x => x + 1, x => x + 2)).toBe(4)
	expect(call.do(1, x => x + 1, 10, (x, y) => [x, y])).toEqual([10, [1, 2, 10]])
	expect(await call.do(Promise.resolve(1), async p => (await p) + 1, p => p.then(x => x + 2))).toBe(4)
})

test("unew", () => {
	const map = call.unew(Map)([["a", 1], ["b", 2]])
	expect(map).toBeInstanceOf(Map)
	expect(map.get("a")).toBe(1)
})

test("rec", () => {
	const val = call.rec((self, i) => (i ? self(i - 1) + i : 0))(10)
	expect(val).toBe(55)
})

test("syncAsync", () => {
	expect(call.syncAsync(1, 2, 3, (a, b, c) => a + b + c)).toBe(6)

	const waitValue = (ms, value) => new Promise(r => setTimeout(r, ms, value))
	const promise = call.syncAsync(waitValue(400, 1), waitValue(300, 2), 3, (a, b, c) => a + b + c)
	expect(promise).toBeInstanceOf(Promise)
	expect(promise).resolves.toBe(6)
})

test("delay idle1", async () => {
	let a = []
	const trigger = call.delay(type => a.push({ type, time: Date.now() }), 1000, 3000)
	const t = Date.now()
	trigger()
	await new Promise(r => setTimeout(r, 1500))

	expect(a.length).toBe(1)
	expect(a[0].type).toBe("idle")
	expect(about(a[0].time - t, 1000, { diff: 100 })).toBe(true)
})

test("delay idle2", async () => {
	let a = []
	const trigger = call.delay(type => a.push({ type, time: Date.now() }), 100, 0)
	const t = Date.now()

	// idle より短い間隔で trigger を呼び出し続ける
	trigger()
	while (Date.now() - t < 1500) {
		await new Promise(r => setTimeout(r, 50))
		trigger()
	}
	await new Promise(r => setTimeout(r, 500))

	expect(a.length).toBe(1)
	expect(a[0].type).toBe("idle")
	expect(a[0].time - t > 1500).toBe(true)
})

test("delay cancel", async () => {
	let a = []
	const trigger = call.delay(type => a.push({ type, time: Date.now() }), 1000, 3000)
	const t = Date.now()
	trigger()
	trigger.cancel()
	await new Promise(r => setTimeout(r, 1500))

	expect(a.length).toBe(0)
})

test("delay timeout", async () => {
	let a = []
	const trigger = call.delay(type => a.push({ type, time: Date.now() }), 100, 1000)
	const t = Date.now()

	// idle より短い間隔で trigger を呼び出し続ける
	trigger()
	while (Date.now() - t < 1500) {
		await new Promise(r => setTimeout(r, 50))
		trigger()
	}
	await new Promise(r => setTimeout(r, 500))

	expect(a.length).toBe(2)
	expect(a[0].type).toBe("timeout")
	expect(a[1].type).toBe("idle")
	expect(about(a[0].time - t, 1000, { diff: 100 })).toBe(true)
})

test("for", async () => {
	{
		let i = 0
		call.for(3, () => i++)
		expect(i).toBe(3)
	}
	{
		let i = 0
		const exit_index = call.for(
			10,
			index => {
				i++
				if (index === 5) return true
			},
			{ enable_exit: true }
		)
		expect(exit_index).toBe(5)
		expect(i).toBe(6)
	}
	{
		// enable_exit がない場合は早期終了できない
		let i = 0
		const exit_index = call.for(10, index => {
			i++
			if (index === 5) return true
		})
		expect(exit_index).toBe(undefined)
		expect(i).toBe(10)
	}
	{
		const returns = call.for(3, x => x, { enable_return: true })
		expect(returns).toEqual([0, 1, 2])
	}
	{
		const returns = call.for(3, x => x, { enable_return: true, no_arguments: true })
		expect(returns).toEqual([undefined, undefined, undefined])
	}
})

test("limit", () => {
	expect(call.limit((...a) => a, 0)(1, 2, 3)).toEqual([])
	expect(call.limit((...a) => a, 1)(1, 2, 3)).toEqual([1])
	expect(["10", "20", "30"].map(call.limit(parseInt, 1))).toEqual([10, 20, 30])
})
