const error = require("../lib/error.js")

test("error", () => {
	const detail = { z: 1, x: "aaa" }
	const err = error("CODE", "TEXT", detail)
	expect(err).toBeInstanceOf(Error)
	expect(err.code).toBe("CODE")
	expect(err.text).toBe("TEXT")
	expect(err.detail).toBe(detail)
	expect(err.message).toBe("[CODE] TEXT")
})
