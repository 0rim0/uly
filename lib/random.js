const loop = require("./loop.js")

module.exports = {
	frand(from, to) {
		if (to == null) {
			to = from
			from = 0
		}
		return Math.random() * (to - from) + from
	},

	irand(from, to) {
		return ~~module.exports.frand(from, to)
	},

	brand() {
		return Math.random() < 0.5
	},

	srand(num) {
		const sign = module.exports.irand(2) * 2 - 1
		return sign * num || sign
	},

	str(length, base = 36) {
		let str = ""
		length = ~~length
		while (str.length < length)
			str += Math.random()
				.toString(base)
				.slice(2)
		return str.slice(0, length)
	},

	strWithTime(length, opt) {
		opt = opt || {}
		const time_first = opt.time_pos === "before"
		const joiner = opt.joiner == null ? "_" : opt.joiner
		const base = ~~opt.base || 36
		const time = Date.now().toString(base)
		const rand = module.exports.str(length)
		return (time_first ? [time, rand] : [rand, time]).join(joiner)
	},

	pick(arr, count, can_duplicate) {
		if (count > 0) {
			if (can_duplicate) {
				return loop(count).map(module.exports.pick.bind(null, arr))
			} else {
				const copy = arr.slice()
				const result = []
				loop(Math.min(count, copy.length)).for(() => {
					const idx = ~~(Math.random() * copy.length)
					result.push(copy[idx])
					const t = copy.pop()
					if (idx < copy.length) copy[idx] = t
				})
				return result
			}
		} else {
			return arr[~~(Math.random() * arr.length)]
		}
	},
}
