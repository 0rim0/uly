const regexp = require("./regexp.js")

module.exports = {
	replaceText(str, from, to) {
		return str.replace(new RegExp(regexp.escape(from), "g"), to)
	},

	splitChars(str, num) {
		const n = ~~num <= 0 ? Infinity : ~~num
		let s = String(str)
		const result = []
		while (s && result.length < n - 1) {
			const cp = s.codePointAt()
			const size = cp > 65535 ? 2 : 1
			result.push(s.slice(0, size))
			s = s.slice(size)
		}
		if (s) result.push(s)
		return result
	},

	split(str, splitter, num) {
		const n = ~~num <= 0 ? Infinity : ~~num
		let s = String(str)
		if (splitter instanceof RegExp) {
			if (splitter.test("")) return module.exports.splitChars(str, num)

			const result = []
			let len = 0
			while (s && len < n - 1) {
				const matched = s.match(splitter)
				if (!matched) {
					result.push(s)
					return result
				} else {
					result.push(s.slice(0, matched.index), ...matched.slice(1))
					s = s.slice(matched.index + matched[0].length)
					len++
				}
			}
			result.push(s)
			return result
		} else {
			const sp = String(splitter)
			if (sp === "") return module.exports.splitChars(str, num)

			const result = []
			while (s && result.length < n - 1) {
				const index = s.indexOf(sp)
				if (index < 0) {
					result.push(s)
					return result
				} else {
					result.push(s.slice(0, index))
					s = s.slice(index + sp.length)
				}
			}
			result.push(s)
			return result
		}
	},

	firstDiff(s1, s2) {
		if (s1 === s2) return -1
		let i = 0
		while (s1[i] === s2[i]) i++
		return i
	},

	fixedLeft(str, length, pad_chars = "0") {
		return String(str)
			.padEnd(length, pad_chars)
			.slice(0, length)
	},

	fixedRight(str, length, pad_chars = "0") {
		return String(str)
			.padStart(length, pad_chars)
			.slice(-length)
	},
}
