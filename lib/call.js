module.exports = {
	createWhen(ifTrue, ifFalse) {
		return (value, ...extras) =>
			value
				? typeof ifTrue === "function"
					? ifTrue(value, ...extras)
					: ifTrue
				: typeof ifFalse === "function"
					? ifFalse(value, ...extras)
					: ifFalse
	},

	when(value, ifTrue, ifFalse) {
		return module.exports.createWhen(ifTrue, ifFalse)(value)
	},

	createCase(map) {
		const [get, getDefault] = map.get ? [k => map.get(k), () => map.get(null)] : [k => map[k], () => map["default"]]

		return (value, ...extras) => {
			const fn = get(value)
			if (fn) {
				return fn.call(map, value, ...extras)
			}
			const default_fn = getDefault()
			if (default_fn) {
				return default_fn.call(map, value, ...extras)
			}
			throw new Error("no matched and default function")
		}
	},

	case(value, map) {
		return module.exports.createCase(map)(value)
	},

	do(...fns) {
		const results = []
		for (const fn of fns) {
			if (typeof fn === "function") {
				results.push(fn(results[results.length - 1], [...results]))
			} else {
				results.push(fn)
			}
		}
		return results[results.length - 1]
	},

	unew(ctor) {
		return (...a) => new ctor(...a)
	},

	rec(fn) {
		const caller = (...x) => bound(...x)
		const bound = fn.bind(null, caller)
		return bound
	},

	syncAsync(...args) {
		const fn = args.pop()
		return args.some(a => a instanceof Promise) ? Promise.all(args).then(v => fn(...v)) : fn(...args)
	},

	delay(fn, idle_time, timeout_time) {
		idle_time = idle_time == null ? 500 : +idle_time || 0
		timeout_time = +timeout_time || 0

		let idle_timer
		let timeout_timer

		const onIdle = () => {
			clearTimeout(timeout_timer)
			timeout_timer = null
			fn("idle")
		}
		const onTimeout = () => {
			fn("timeout")
		}

		const trigger = () => {
			clearTimeout(idle_timer)
			idle_timer = setTimeout(onIdle, idle_time)
			if (!timeout_timer && timeout_time) {
				timeout_timer = setTimeout(onTimeout, timeout_time)
			}
		}
		trigger.cancel = () => {
			clearTimeout(idle_timer)
			clearTimeout(timeout_timer)
			idle_timer = null
			timeout_timer = null
		}
		return trigger
	},

	for(times, fn, { enable_exit, enable_return, no_arguments } = {}) {
		times = ~~times
		const returns = []
		for (let i = 0; i < times; i++) {
			const result = no_arguments ? fn() : fn(i, times)
			returns.push(result)
			if (enable_exit && result === true) return i
		}
		if (enable_return) return returns
	},

	limit(fn, num) {
		return function(...a) {
			return fn.apply(this, a.slice(0, ~~num))
		}
	},
}
