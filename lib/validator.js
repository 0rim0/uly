const valid = (result, messages) => {
	return {
		valid: true,
		result,
		messages: messages || [],
	}
}

const invalid = messages => {
	return {
		valid: false,
		messages,
	}
}

const validInfo = (result, body, value) => {
	return valid(result, [infoMessage(body, value)])
}

const invalidError = (reason, value) => {
	return invalid([errorMessage(reason, value)])
}

const infoMessage = (body, value) => {
	return {
		type: "info",
		body,
		value,
	}
}

const errorMessage = (reason, value) => {
	return {
		type: "error",
		body: reason,
		value,
	}
}

const containerMessage = (value, body, messages) => {
	return {
		type: "container",
		body,
		messages,
		value,
	}
}

const validator = {
	and(...fns) {
		return value => {
			let v = value
			const messages = []
			for (const [index, fn] of fns.entries()) {
				const result = fn(v)
				if (result.messages.length) {
					messages.push(containerMessage(v, `and#${index}`, result.messages))
				}
				v = result.result
				if (!result.valid) return invalid(messages)
			}

			return valid(v, messages)
		}
	},

	or(...fns) {
		return value => {
			const messages = []
			for (const [index, fn] of fns.entries()) {
				const result = fn(value)
				if (result.messages.length) {
					messages.push(containerMessage(value, `or#${index}`, result.messages))
				}
				if (result.valid) return valid(result.result, messages)
			}
			return invalid(messages)
		}
	},

	typeof(type) {
		return value => {
			const valtype = value === null ? "null" : Array.isArray(value) ? "array" : typeof value
			const is_valid = valtype === type
			return is_valid
				? valid(value)
				: invalidError(`[typeof] ${type} type is expected, but value type is ${valtype}`, value)
		}
	},

	instanceof(ctor) {
		return value => {
			const is_valid = value instanceof ctor
			return is_valid ? valid(value) : invalidError(`[instanceof] value is not instance of ${ctor.name}`, value)
		}
	},

	is(...values) {
		return value => {
			const is_valid = values.includes(value)
			return is_valid ? valid(value) : invalidError("[is] value is not matched", value)
		}
	},

	not(fn) {
		return value => {
			const result = fn(value)
			const is_valid = !result.valid
			return is_valid ? valid(value) : invalidError("[not] value matched sub condition", value)
		}
	},

	range({ min, max, exclude_min, exclude_max }) {
		return value => {
			if (isNaN(value)) {
				return invalidError("[range] value is not a number", value)
			}
			if (min != null && (exclude_min ? value <= min : value < min)) {
				return invalidError(`[range] ${value} is less than min (${min})`, value)
			}
			if (max != null && (exclude_max ? value >= max : value > max)) {
				return invalidError(`[range] ${value} is greater than max (${max})`, value)
			}
			return valid(value)
		}
	},

	length({ min, max, exclude_min, exclude_max }) {
		return value => {
			const len = value && value.length
			if (isNaN(len)) {
				return invalidError("[length] length property of value is not a number", value)
			}
			if (min != null && (exclude_min ? len <= min : len < min)) {
				return invalidError(`[length] ${len} is less than min (${min})`, value)
			}
			if (max != null && (exclude_max ? len >= max : len > max)) {
				return invalidError(`[length] ${len} is greater than max (${max})`, value)
			}
			return valid(value)
		}
	},

	arrayof(opt) {
		let defaultFn
		let items
		let check_all = false
		if (typeof opt === "function") {
			defaultFn = opt
			items = []
		} else if (Array.isArray(opt)) {
			defaultFn = value => valid(value)
			items = opt
		} else {
			defaultFn = opt.defaultFn
			items = opt.items
			check_all = !!opt.check_all
		}
		return value => {
			let is_valid = true
			const messages = []
			const result_items = []
			for (const [index, item] of value.entries()) {
				const fn = items[index] || defaultFn
				const result = fn(item)
				is_valid = is_valid && result.valid
				if (result.messages.length) {
					messages.push(containerMessage(value, `array#${index}`, result.messages))
				}
				result_items[index] = result.result
				if (!check_all && !valid) break
			}
			return is_valid ? valid(result_items, messages) : invalid(messages)
		}
	},

	property(properties, opt = {}) {
		return value => {
			let is_valid = true
			const messages = []
			const result_object = {}
			for (const key of new Set([...Object.keys(value), ...Object.keys(properties)])) {
				const val = value[key]
				let fn = properties[key]
				if (!fn && opt.dprop) {
					fn = opt.dprop(key)
				}
				if (fn) {
					const result = fn(val)
					is_valid = is_valid && result.valid
					if (result.messages.length) {
						messages.push(containerMessage(value, `property#${key}`, result.messages))
					}
					result_object[key] = result.result
					if (!opt.check_all && !valid) break
				} else {
					if (opt.allow_extra_key) {
						result_object[key] = val
					} else {
						is_valid = false
						messages.push(errorMessage(`property#${key}: unallowed extra key`, value))
						if (!opt.check_all) break
					}
				}
			}
			return is_valid ? valid(result_object, messages) : invalid(messages)
		}
	},

	unique({ convert } = {}) {
		return value => {
			const uniq = [...new Set(value)]
			const is_valid = uniq.length === value.length
			return is_valid
				? valid(value)
				: convert
					? validInfo(uniq, "[unique] converted", value)
					: invalidError("[unique] not unique", value)
		}
	},

	int32({ convert } = {}) {
		return value => {
			const i32 = ~~value
			const is_valid = i32 === value
			if (is_valid) {
				return valid(value)
			}
			if (convert === "force") {
				return validInfo(i32, "[int32] converted", value)
			}
			if (convert) {
				const convertable = !isNaN(value) && (+value === 0 ? String(value) === "0" : true)
				return convertable ? validInfo(i32, "[int32] converted", value) : invalidError("[int32] value is not int32")
			}
			return invalidError("[int32] value is not int32", value)
		}
	},

	int({ convert } = {}) {
		return value => {
			const int = Math.min(Number.MAX_SAFE_INTEGER, Math.max(Number.MIN_SAFE_INTEGER, Math.trunc(value) || 0))
			const is_valid = int === value
			if (is_valid) {
				return valid(value)
			}
			if (convert === "force") {
				return validInfo(int, "[int] conveted", value)
			}
			if (convert) {
				const convertable = !isNaN(value) && (+value === 0 ? String(value) === "0" : true)
				return convertable ? validInfo(int, "[int] converted", value) : invalidError("[int] value is not int")
			}
			return invalidError("[int] value is not int", value)
		}
	},

	num({ convert } = {}) {
		return value => {
			const num = +value || 0
			const is_valid = num === value
			if (is_valid) {
				return valid(value)
			}
			if (convert === "force") {
				return validInfo(num, "[num] conveted", value)
			}
			if (convert) {
				const convertable = !isNaN(value) && (+value === 0 ? String(value) === "0" : true)
				return convertable ? validInfo(num, "[num] converted", value) : invalidError("[num] value is not num")
			}
			return invalidError("[num] value is not num", value)
		}
	},

	nan({ is_nan, convert, alternative } = {}) {
		return value => {
			if (is_nan) {
				return isNaN(value) ? valid(value) : invalidError("[nan] value is not NaN", value)
			} else {
				const is_valid = !isNaN(value)
				return is_valid
					? valid(value)
					: convert
						? validInfo(alternative, "[nan] convert", value)
						: invalidError("[nan] value is NaN", value)
			}
		}
	},

	numstring({ convert } = {}) {
		return value => {
			const str = String(+value || 0)
			const is_valid = str === value
			if (is_valid) {
				return valid(value)
			}
			if (convert === "force") {
				return validInfo(str, "[numstring] conveted", value)
			}
			if (convert) {
				const convertable = !isNaN(value) && (+value === 0 ? String(value) === "0" : true)
				return convertable
					? validInfo(str, "[numstring] converted", value)
					: invalidError("[numstring] value is not numstring")
			}
			return invalidError("[numstring] value is not numstring", value)
		}
	},

	url({ base_url, accept_scheme, convert } = {}) {
		return value => {
			try {
				const url = new URL(value, base_url)
				if (accept_scheme) {
					const schemes = Array.isArray(accept_scheme) ? accept_scheme : [accept_scheme]
					if (!schemes.includes(url.protocol.slice(0, -1))) {
						return invalidError("[url] value is not matched to accept_scheme")
					}
				}
				const is_valid = url.toString() === value
				return is_valid
					? valid(value)
					: convert
						? validInfo(url.toString(), "[url] converted", value)
						: invalidError("[url] value is fixable invalid url")
			} catch {
				return invalidError("[url] value is invalid url")
			}
		}
	},

	match(regexp) {
		return value => {
			const is_valid = regexp.test(value)
			return is_valid ? valid(value) : invalidError("[match] value is not matched to regexp", value)
		}
	},

	when(...conds) {
		return value => {
			for (const cond of conds) {
				if (typeof cond === "function") {
					return cond(value)
				}
				const [cfn, fn] = cond
				if (cfn(value)) {
					return fn(value)
				}
			}
			return invalidError("[when] no matching conditions found", value)
		}
	},

	cases(cfn, fns) {
		return value => {
			const fn = fns[cfn(value)]
			if (typeof fn === "function") {
				return fn(value)
			} else {
				return invalidError("[cases] no matching conditions found", value)
			}
		}
	},

	nullish(...additional) {
		return validator.is(null, undefined, ...additional)
	},

	nullable(fn) {
		return validator.or(validator.nullish(), fn)
	},

	number(opt = {}) {
		const fns = []

		if (opt.numstring) {
			fns.push(validator.or(validator.num({ convert: true }), validator.typeof("number")))
		} else {
			fns.push(validator.typeof("number"))
		}

		if ("min" in opt || "max" in opt) {
			const { min, max, exclude_min, exclude_max } = opt
			fns.push(validator.range({ min, max, exclude_min, exclude_max }))
		}

		if (opt.int32) {
			fns.push(validator.int32)
		} else if (opt.int) {
			fns.push(validator.int)
		}

		const validate = validator.and(...fns)
		return opt.nullable ? validator.nullable(validate) : validate
	},

	string(opt = {}) {
		const fns = [validator.typeof("string")]

		if ("length" in opt) {
			fns.push(validator.length({ min: opt.length, max: opt.length }))
		} else if ("min" in opt || "max" in opt) {
			const { min, max, exclude_min, exclude_max } = opt
			fns.push(validator.length({ min, max, exclude_min, exclude_max }))
		}

		if (opt.regexp) {
			fns.push(validator.match(opt.regexp))
		}

		const validate = validator.and(...fns)
		return opt.nullable ? validator.nullable(validate) : validate
	},

	boolean(opt = {}) {
		const validate = validator.typeof("boolean")
		return opt.nullable ? validator.nullable(validate) : validate
	},

	function(opt = {}) {
		const validate = validator.typeof("function")
		return opt.nullable ? validator.nullable(validate) : validate
	},

	array(opt = {}) {
		const fns = [validator.typeof("array")]

		if ("length" in opt) {
			fns.push(validator.length({ min: opt.length, max: opt.length }))
		} else if ("min" in opt || "max" in opt) {
			const { min, max, exclude_min, exclude_max } = opt
			fns.push(validator.length({ min, max, exclude_min, exclude_max }))
		}

		if (opt.items) {
			fns.push(validator.arrayof(opt.items))
		}

		const validate = validator.and(...fns)
		return opt.nullable ? validator.nullable(validate) : validate
	},

	object(opt = {}) {
		const fns = [validator.typeof("object")]

		if (opt.properties) {
			fns.push(validator.property(opt.properties, opt.properties_opt))
		}

		const validate = validator.and(...fns)
		return opt.nullable ? validator.nullable(validate) : validate
	},

	date(opt = {}) {
		const fns = [validator.instanceof(Date), validator.nan()]

		if ("min" in opt || "max" in opt) {
			const { min, max, exclude_min, exclude_max } = opt
			fns.push(validator.range({ min, max, exclude_min, exclude_max }))
		}

		const validate = validator.and(...fns)
		return opt.nullable ? validator.nullable(validate) : validate
	},
}

module.exports = { valid, invalid, validInfo, invalidError, validator }
