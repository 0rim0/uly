module.exports = {
	sum(...nums) {
		return nums.reduce((a, e) => a + (+e || 0), 0)
	},

	average(...nums) {
		return module.exports.sum(...nums) / nums.length
	},

	median(...nums) {
		if (nums.length === 0) return NaN
		const sorted = nums.map(e => +e).sort()
		if (nums.length % 2) {
			return sorted[~~(nums.length / 2)]
		} else {
			return (sorted[nums.length / 2 - 1] + sorted[nums.length / 2]) / 2
		}
	},

	cap(num, min, max) {
		if (typeof min === "number") num = Math.max(min, num)
		if (typeof max === "number") num = Math.min(max, num)
		return num
	},

	about(base_num, num, cond) {
		let min_num, max_num
		if (!cond) cond = 10
		if (typeof cond === "number") cond = { percent: cond }

		if (typeof cond.min_num === "number" && typeof cond.max_num === "number") {
			min_num = cond.min_num
			max_num = cond.max_num
		} else if (typeof cond.diff === "number") {
			min_num = base_num - cond.diff
			max_num = base_num + cond.diff
		} else if (typeof cond.min_diff === "number" && typeof cond.max_diff === "number") {
			min_num = base_num - cond.min_diff
			max_num = base_num + cond.max_diff
		} else if (typeof cond.percent === "number") {
			min_num = (base_num * (100 - cond.percent)) / 100
			max_num = (base_num * (100 + cond.percent)) / 100
		} else if (typeof cond.min_percent === "number" && typeof cond.max_percent === "number") {
			min_num = (base_num * cond.min_percent) / 100
			max_num = (base_num * cond.max_percent) / 100
		}

		return min_num <= num && num <= max_num
	},
}
