module.exports = (code, text, detail) => {
	const err = new RichError(`[${code}] ${text}`)
	err.code = code
	err.text = String(text || "")
	err.detail = detail
	return err
}

class RichError extends Error {
	code = null
	text = null
	detail = null
}
