module.exports = {
	escape(str) {
		return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
	},
}
