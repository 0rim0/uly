export default {
	has(key) {
		return key in localStorage
	},
	get(key, default_value) {
		const json = localStorage[key]
		return json ? JSON.parse(json) : default_value
	},
	set(key, value) {
		if (value === undefined) {
			this.remove(key)
			return
		}
		localStorage[key] = JSON.stringify(value)
	},
	remove(key) {
		localStorage.removeItem(key)
	},
}
