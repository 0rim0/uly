module.exports = {
	/// a タグを使ってファイルをダウンロード
	downloadURL(url, name) {
		const a = document.createElement("a")
		a.href = url
		a.download = name
		a.click()
	},

	/// arraybuffer, blob, string, 他 json 化可能な値をダウンロード
	download(value, name) {
		const blob = (() => {
			// arraybuffer
			{
				let buffer
				if (value instanceof ArrayBuffer) {
					buffer = value
				} else if (value instanceof Object && value.buffer instanceof ArrayBuffer) {
					buffer = value.buffer
				}

				if (buffer) return new Blob([buffer], { type: "application/octet-stream" })
			}
			// Blob
			if (value instanceof Blob) return value
			// string and other
			{
				const str = typeof value === "string" ? value : JSON.stringify(value, null, "\t")
				return new Blob([str], { type: "text/plain" })
			}
		})()

		const url = URL.createObjectURL(blob)
		module.exports.downloadURL(url, name)

		// メモリ解放
		// ブラウザローカルからのダウンロードで 1 分超えることはまずないと思うのでとりあえず 1 分
		setTimeout(() => URL.revokeObjectURL(url), 1000 * 60)
	},

	async openFileDialog(multiple, accept) {
		return new Promise(resolve => {
			const input = document.createElement("input")
			input.type = "file"
			if (multiple) input.multiple = true
			if (accept) input.accept = accept
			input.addEventListener("change", eve => {
				const files = eve.target.files
				if (multiple) {
					resolve([...files])
				} else {
					resolve(files[0] || null)
				}
			})
			input.click()
		})
	},

	async readFile(file, as) {
		return new Promise((resolve, reject) => {
			const fr = new FileReader()
			fr.onload = async eve => {
				resolve(fr.result)
			}
			fr.onerror = eve => {
				reject(fr.error)
			}
			const methods = {
				arraybuffer: "readAsArrayBuffer",
				dataurl: "readAsDataURL",
				text: "readAsText",
			}
			fr[methods[as]](file)
		})
	},
}
