const string = require("./string.js")

module.exports = {
	parse(text, { col_delimiter = ",", quote = '"', row_delimiter = "\n" } = {}) {
		const result = []
		const row = []
		let i = 0
		while (true) {
			if (text.startsWith(quote, i)) {
				// quote case
				const si = i + quote.length
				i = si
				while (true) {
					const qi = text.indexOf(quote, i)
					if (qi === -1) {
						// invalid case
						row.push(text.slice(si))
						result.push(row.slice())
						return result
					}
					if (text.startsWith(quote + quote, qi)) {
						// escaped quote
						i = qi + quote.length * 2
						continue
					}
					if (text.startsWith(quote + col_delimiter, qi)) {
						// found quote end and col delimiter
						row.push(string.replaceText(text.slice(si, qi), quote + quote, quote))
						i = qi + quote.length + col_delimiter.length
						break
					}
					if (text.startsWith(quote + row_delimiter, qi)) {
						// found quote end and row delimiter
						row.push(string.replaceText(text.slice(si, qi), quote + quote, quote))
						result.push(row.slice())
						row.length = 0
						i = qi + quote.length + row_delimiter.length
						break
					}
					if (text.length === qi + quote.length) {
						// found quote and eof
						row.push(string.replaceText(text.slice(si, qi), quote + quote, quote))
						result.push(row.slice())
						return result
					}
					// found quote end but no delimiter
					i = qi + quote.length
				}
			} else {
				// no quote case
				const cdi = text.indexOf(col_delimiter, i)
				const rdi = text.indexOf(row_delimiter, i)
				if (cdi === -1 && rdi === -1) {
					// standard end
					row.push(text.slice(i))
					result.push(row.slice())
					return result
				}
				if (cdi === -1 || (rdi > -1 && cdi > rdi)) {
					// found row delimiter
					row.push(text.slice(i, rdi))
					result.push(row.slice())
					row.length = 0
					i = rdi + row_delimiter.length
				} else {
					// found col delimiter
					row.push(text.slice(i, cdi))
					i = cdi + col_delimiter.length
				}
			}
		}
	},

	build(aoa, { col_delimiter = ",", quote = '"', shouldQuote, row_delimiter = "\n" } = {}) {
		shouldQuote =
			shouldQuote || (text => text.includes(col_delimiter) || text.includes(row_delimiter) || text.startsWith(quote))
		return aoa
			.map(row =>
				row
					.map(cell => {
						let text = String(cell)
						return shouldQuote(text) ? quote + string.replaceText(text, quote, quote + quote) + quote : text
					})
					.join(col_delimiter)
			)
			.join(row_delimiter)
	},
}
