const number = require("./number.js")

module.exports = class {
	constructor(items, per_page, overlap, initial_page) {
		this.items = items || []
		this.per_page = ~~per_page <= 0 ? 10 : ~~per_page
		this.overlap = ~~overlap < 0 ? 0 : ~~overlap
		if(this.per_page <= this.overlap) throw new Error("overlap must be less than per_page")
		this.page = 0
		this.jump(initial_page)
	}

	getPageItems(page) {
		return this.items.slice(this.getStartIndex(page), this.getEndIndex(page))
	}

	getStartIndex(page) {
		page = page == null ? this.page : page
		return page * (this.per_page - this.overlap)
	}

	getEndIndex(page) {
		page = page == null ? this.page : page
		return this.getStartIndex(page) + this.per_page
	}

	getLastPage() {
		const n = this.items.length
		const p = this.per_page
		const o = this.overlap
		if(n < p) return 0
		return Math.ceil((n - p) / (p - o))
	}

	hasNext() {
		return this.page < this.getLastPage()
	}

	hasPrev() {
		return this.page > 0
	}

	next() {
		if (this.hasNext()) this.page++
		return this.page
	}

	prev() {
		if (this.hasPrev()) this.page--
		return this.page
	}

	jump(page) {
		this.page = number.cap(~~page, 0, this.getLastPage())
	}
}
