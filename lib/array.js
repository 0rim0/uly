module.exports = {
	loop(arr) {
		return {
			pos: 0,
			get(force_index) {
				if (force_index || force_index === 0) {
					return this.arr[force_index]
				}
				const pos = this.pos
				this.pos = ++this.pos % this.arr.length
				return this.arr[pos]
			},
			reset() {
				this.pos = 0
			},
			arr,
		}
	},

	wrap(arr) {
		return Array.isArray(arr) ? arr : [arr]
	},

	selectKV(index, item, key_sel, value_sel, excludes_key) {
		const k = typeof key_sel === "function" ? key_sel(item, index) : item[key_sel]
		let v = item
		if (excludes_key) {
			const { [key_sel]: _, ...rem } = item
			v = rem
		} else if (value_sel) {
			v = typeof value_sel === "function" ? value_sel(item, index) : item[value_sel]
		}
		return [k, v]
	},

	toObject(arr, key, { excludes_key, value } = {}) {
		return Object.fromEntries(
			arr.map((e, i) => {
				return this.selectKV(i, e, key, value, excludes_key)
			})
		)
	},

	groupBy(arr, key, { excludes_key, value } = {}) {
		const result = {}
		for (const [index, item] of arr.entries()) {
			const [k, v] = this.selectKV(index, item, key, value, excludes_key)
			if (result[k]) {
				result[k].push(v)
			} else {
				result[k] = [v]
			}
		}
		return result
	},

	diff(arr1, arr2, selector1, selector2) {
		const s1 = selector1 == null ? x => x : typeof selector1 === "function" ? selector1 : x => x[selector1]
		const s2 = selector2 == null ? s1 : typeof selector2 === "function" ? selector2 : x => x[selector2]
		let rem = arr1.map(e => [e, s1(e)])
		for (const aitem of arr2) {
			const ditem = s2(aitem)
			rem = rem.filter(item => ditem !== item[1])
		}
		return rem.map(e => e[0])
	},

	intersect(arr1, arr2, selector1, selector2) {
		const s1 = selector1 == null ? x => x : typeof selector1 === "function" ? selector1 : x => x[selector1]
		const s2 = selector2 == null ? s1 : typeof selector2 === "function" ? selector2 : x => x[selector2]
		const result = []
		const as2 = arr2.map(e => s2(e))
		for (const aitem of arr1) {
			const citem = s1(aitem)
			if (as2.includes(citem)) {
				result.push(aitem)
			}
		}
		return result
	},

	sort(arr, ...bys) {
		const formated_bys = bys.map(by => {
			if (typeof by === "string" || typeof by === "function") {
				return { by, order: "asc" }
			}
			if (Array.isArray(by)) {
				return { by: by[0], order: String(by[1] || "asc").toLowerCase() }
			}
			if (by) {
				return { by: by.by, order: String(by.order || "asc").toLowerCase() }
			}
			return { by: null, order: "asc" }
		})
		const order_sign = { asc: 1, desc: -1 }
		return arr.sort((x, y) => {
			for (const { by, order } of formated_bys) {
				const [xv, yv] = typeof by === "string" ? [x[by], y[by]] : typeof by === "function" ? [by(x), by(y)] : [x, y]
				if (xv < yv) return order_sign[order] * -1
				if (xv > yv) return order_sign[order] * 1
			}
			return 0
		})
	},

	find(arr, cb) {
		const index = arr.findIndex(cb)
		const found = index >= 0
		const value = arr[index]
		return { index, found, value }
	},

	lastFind(arr, cb) {
		for (let i = arr.length - 1; i >= 0; i--) {
			if (cb(arr[i], i, arr)) {
				return { index: i, found: true, value: arr[i] }
			}
		}
		return { index: -1, found: false, value: undefined }
	},

	max(arr) {
		let indexes
		let value
		for (const [i, v] of arr.entries()) {
			if (!indexes) {
				indexes = [i]
				value = v
			} else if (value < v) {
				indexes = [i]
				value = v
			} else if (value === v) {
				indexes.push(i)
			}
		}
		return { indexes, value }
	},

	min(arr) {
		let indexes
		let value
		for (const [i, v] of arr.entries()) {
			if (!indexes) {
				indexes = [i]
				value = v
			} else if (value > v) {
				indexes = [i]
				value = v
			} else if (value === v) {
				indexes.push(i)
			}
		}
		return { indexes, value }
	},

	remove(arr, condition, all) {
		condition = condition || {}
		let removed_num = 0
		for (let i = 0; i < arr.length; ) {
			if (removed_num === 1 && !all) break
			if ("value" in condition) {
				if (condition.value === arr[i]) {
					arr.splice(i, 1)
					removed_num++
					continue
				}
			}
			if ("index" in condition) {
				if (Array.isArray(condition.index)) {
					if (condition.index.includes(i + removed_num)) {
						arr.splice(i, 1)
						removed_num++
						continue
					}
				} else {
					if (condition.index === i + removed_num) {
						arr.splice(i, 1)
						removed_num++
						continue
					}
				}
			}
			if ("match" in condition) {
				if (condition.match(arr[i], i + removed_num)) {
					arr.splice(i, 1)
					removed_num++
					continue
				}
			}
			i++
		}
		return arr
	},

	repeat(arr, num) {
		let result = []
		for (let i = 0; i < ~~num; i++) result.push(...arr)
		return result
	},

	slices(arr, lengths) {
		let i = 0
		const result = []
		for (const length of lengths) {
			result.push(arr.slice(i, i + ~~length))
			i += ~~length
		}
		return result
	},

	reader(list, max_repeat) {
		list = typeof list.next === "function" ? [...list] : list
		max_repeat = +max_repeat || 0
		let index = 0
		let repeat = 0

		return read_num => {
			if (list.length === 0) return items.slice(0, 0)
			const num = ~~read_num || 1
			let result = list.slice(index, index + num)
			index += num
			while (result.length < num) {
				if (++repeat > max_repeat) break
				index = 0
				const shortage = num - result.length
				const part = list.slice(index, index + shortage)
				result = result.concat(part)
				index += shortage
			}
			return result
		}
	},

	get(arr, index) {
		if (!Array.isArray(index)) {
			let i = ~~index
			if (i < 0) i = arr.length + i
			return arr[i]
		}
		return index.map(i => {
			if (!Array.isArray(i)) {
				return module.exports.get(arr, i)
			} else {
				const [from, to] = i
				return arr.slice(from, to)
			}
		})
	},

	paste(arr, pos, from, allow_extend) {
		pos = Math.max(0, ~~pos)
		for (let i = 0; i < from.length; i++) {
			if (allow_extend || pos + i < arr.length) {
				arr[pos + i] = from[i]
			} else {
				break
			}
		}
		return arr
	},
}
