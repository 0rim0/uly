module.exports = n => new Loop(n)

class Loop {
	constructor(num) {
		this.num = ~~num
	}

	for(fn) {
		for (let i = 0; i < this.num; i++) {
			if (fn(i, this.num) === true) return
		}
	}

	map(fn) {
		const result = []
		for (let i = 0; i < this.num; i++) {
			result.push(fn(i, this.num))
		}
		return result
	}
}
