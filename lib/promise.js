module.exports = {
	create() {
		let resolve, reject
		const promise = new Promise((r1, r2) => {
			resolve = r1
			reject = r2
		})
		return Object.assign(promise, { resolve, reject })
	},

	wait(ms) {
		return new Promise(r => setTimeout(r, ~~ms))
	},

	resolve(p) {
		return p.then(x => ({ ok: true, result: x }), x => ({ ok: false, error: x }))
	},
}
