const array = require("./array.js")

module.exports = {
	format(format, date = new Date()) {
		return format.replace(/yyyy|MM|dd|HH|mm|ss|fff/g, e => {
			if (e === "yyyy") return date.getFullYear()
			if (e === "MM") return String(date.getMonth() + 1).padStart(2, "0")
			if (e === "dd") return String(date.getDate()).padStart(2, "0")
			if (e === "HH") return String(date.getHours()).padStart(2, "0")
			if (e === "mm") return String(date.getMinutes()).padStart(2, "0")
			if (e === "ss") return String(date.getSeconds()).padStart(2, "0")
			if (e === "fff") return String(date.getMilliseconds()).padStart(3, "0")
		})
	},

	add(date, { year, month, day, hour, minute, second, millisecond }) {
		return new Date(
			date.getFullYear() + (year || 0),
			date.getMonth() + (month || 0),
			date.getDate() + (day || 0),
			date.getHours() + (hour || 0),
			date.getMinutes() + (minute || 0),
			date.getSeconds() + (second || 0),
			date.getMilliseconds() + (millisecond || 0)
		)
	},

	modify(date, opt) {
		const current = {
			year: date.getFullYear(),
			month: date.getMonth(),
			day: date.getDate(),
			hour: date.getHours(),
			minute: date.getMinutes(),
			second: date.getSeconds(),
			millisecond: date.getMilliseconds(),
		}
		const resolve = type =>
			typeof opt["a" + type] === "number"
				? opt["a" + type]
				: typeof opt["r" + type] === "number"
					? current[type] + opt["r" + type]
					: current[type]
		return new Date(
			resolve("year"),
			resolve("month"),
			resolve("day"),
			resolve("hour"),
			resolve("minute"),
			resolve("second"),
			resolve("millisecond")
		)
	},

	trunc(date, unit) {
		const units = ["year", "month", "day", "hour", "minute", "second", "millisecond"]
		const index = units.indexOf(unit)
		const args = [
			date.getFullYear(),
			date.getMonth(),
			date.getDate(),
			date.getHours(),
			date.getMinutes(),
			date.getSeconds(),
			date.getMilliseconds(),
		].slice(0, index + 1)
		if (args.length === 1) args.push(0)
		return new Date(...args)
	},

	modifyDate(date, new_date) {
		return new Date(
			new_date.getFullYear(),
			new_date.getMonth(),
			new_date.getDate(),
			date.getHours(),
			date.getMinutes(),
			date.getSeconds(),
			date.getMilliseconds()
		)
	},

	modifyTime(date, new_date) {
		return new Date(
			date.getFullYear(),
			date.getMonth(),
			date.getDate(),
			new_date.getHours(),
			new_date.getMinutes(),
			new_date.getSeconds(),
			new_date.getMilliseconds()
		)
	},

	modifyNearestTime(date, new_date) {
		const same = module.exports.modifyDate(new_date, date)
		const prev = module.exports.modifyDate(new_date, module.exports.add(date, { day: -1 }))
		const next = module.exports.modifyDate(new_date, module.exports.add(date, { day: 1 }))
		const index = array.min([Math.abs(date - same), Math.abs(date - prev), Math.abs(date - next)]).indexes[0]
		return [same, prev, next][index]
	},

	fromTime(hms, date) {
		return new Date(module.exports.format("yyyy/MM/dd", date) + " " + hms)
	},

	today() {
		return module.exports.trunc(new Date(), "day")
	},

	day_msec: 1000 * 3600 * 24,
	week_msec: 1000 * 3600 * 24 * 7,
}
