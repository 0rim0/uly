// logger
// log.level で出力レベルの設定
// log.debug, log.error で各レベルの出力
// デフォルト：
// console.log で出力
// ログ日時をつける
// stringify/format/write は非同期対応

const util = require("util")

const defaults = {
	levels: ["debug", "info", "warn", "error"],
	level: "info",
	stringify(values, level) {
		const texts = values.map(value => (typeof value === "string" ? value : util.inspect(value)))
		if (texts.length === 0) return ""
		// 改行を含む場合は前後の区切りをスペースから改行に変更
		const arr = Array(texts.length * 2 - 1).fill("  ")
		for (const [i, text] of texts.entries()) {
			const index = i * 2
			arr[index] = text
			if (text.includes("\n")) {
				if (arr[index - 1]) arr[index - 1] = "\n"
				if (arr[index + 1]) arr[index + 1] = "\n"
			}
		}
		return arr.join("")
	},
	format(text, level) {
		const now = new Date()
		const timestamp =
			now.getFullYear() +
			"/" +
			String(now.getMonth() + 1).padStart(2, "0") +
			"/" +
			String(now.getDate()).padStart(2, "0") +
			" " +
			String(now.getHours()).padStart(2, "0") +
			":" +
			String(now.getMinutes()).padStart(2, "0") +
			":" +
			String(now.getSeconds()).padStart(2, "0") +
			"." +
			String(now.getMilliseconds()).padStart(3, "0")
		const prefix = `${timestamp} [${level}] `
		const indent = " ".repeat(prefix.length)
		return prefix + text.split("\n").reduce((a, b) => a + "\n" + indent + b)
	},
	write(text) {
		console.log(text)
	},
}

module.exports = (options = {}) => {
	const log = async (level, ...messages) => {
		if (log.levels.indexOf(level) < log.levels.indexOf(log.level)) {
			return
		}

		if (log.console && console[level]) {
			console[level](...messages)
		}
		const text = await log.stringify(messages, level)
		await log.write(await log.format(text, level))
	}

	Object.assign(log, defaults, options)

	// log.debug, log.error などの設定
	for (const level of log.levels) {
		log[level] = log.bind(null, level)
	}

	return log
}
