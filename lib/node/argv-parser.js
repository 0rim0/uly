const makeParamsInfo = params =>
	Object.fromEntries(
		Object.entries(params || {}).map(([k, v]) => {
			if (v.type === String || v.type === Number || v.type === Boolean) {
				const value = {
					type: v.type,
					default: v.default === undefined ? v.type() : v.default,
				}
				return [k, value]
			}
			if (v === String || v === Number || v === Boolean) {
				const value = {
					type: v,
					default: v(),
				}
				return [k, value]
			}
			return [k, v]
		})
	)

const getInfo = (params_info, default_type) => {
	const rec = key => {
		const value = params_info[key]
		if (value == null) return { key, type: default_type, extra: true }
		if (typeof value === "string") return rec(value)
		return { key, ...value }
	}
	return rec
}

const matchStop = (condition, arg) => !!(condition && (condition.test ? condition.test(arg) : condition === arg))

exports.parse = function(option, argv) {
	const args = [...(argv || process.argv.slice(2))]

	const info = getInfo(makeParamsInfo(option.params), option.extra_params)

	const parsed = []
	let key = null
	for (const [index, arg] of args.entries()) {
		if (key === null && matchStop(option.stop, arg)) {
			parsed.push({ remains: args.slice(index) })
			break
		}
		if (matchStop(option.force_stop, arg)) {
			if (key) {
				parsed.push({ key, value: null })
				key = null
			}
			parsed.push({ remains: args.slice(index) })
			break
		}
		if (arg.startsWith("--")) {
			if (key) {
				parsed.push({ key, value: null })
				key = null
			}

			const name = arg.slice(2)
			const sp_index = name.indexOf("=")
			if (sp_index < 0) {
				const type = info(name).type
				if (type === Boolean || type == null) {
					parsed.push({ key: name, value: null })
				} else {
					key = name
				}
			} else {
				parsed.push({ key: name.slice(0, sp_index), value: arg.slice(sp_index + 1) })
			}
			continue
		}
		if (arg.startsWith("-")) {
			if (key) {
				parsed.push({ key, value: null })
				key = null
			}

			const names = arg.slice(1)
			for (let i = 0; i < names.length; i++) {
				const name = names[i]
				if (names[i + 1] === "=") {
					parsed.push({ key: name, value: names.slice(i + 2) })
					break
				}
				const type = info(name).type
				if (type === Boolean || type == null) {
					parsed.push({ key: name, value: null })
				} else if (i + 1 === names.length) {
					key = name
				} else {
					parsed.push({ key: name, value: names.slice(i + 1) })
					break
				}
			}
			continue
		}
		if (key) {
			parsed.push({ key, value: arg })
			key = null
			continue
		}
		parsed.push({ key: null, value: arg })
	}

	if (option.raw) return parsed

	return exports.format(parsed, option)
}

exports.format = (parsed, option) => {
	const params_info = makeParamsInfo(option.params)
	const info = getInfo(params_info, option.extra_params)
	const result = {
		params: Object.fromEntries(
			Object.entries(params_info)
				.filter(([k, v]) => v instanceof Object)
				.map(([k, v]) => [k, v.default])
		),
		args: [],
		remains: [],
	}
	for (const item of parsed) {
		if (item.key === null) {
			result.args.push(item.value)
		} else if (item.remains) {
			result.remains.push(...item.remains)
		} else {
			const { key, type, extra } = info(item.key)
			if (!type && extra) continue
			if (type === Boolean && item.value === null) {
				result.params[key] = true
			}
			if (item.value != null) {
				result.params[key] = type(item.value)
			}
		}
	}
	return result
}
