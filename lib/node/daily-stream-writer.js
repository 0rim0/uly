const fs = require("fs")
const path = require("path")

module.exports = class {
	constructor({ dir = process.cwd(), filename_format = "yyyy-mm-dd.log" } = {}) {
		this.dir = dir
		this.filename_format = filename_format.split(/(yyyy|yy|mm|m|dd|d)/i)
		this.stream = null
		this.day = null
	}

	async write(data) {
		const today = new Date().toLocaleDateString()
		if (!this.stream || this.day !== today) {
			await this.openStream()
			this.day = today
		}
		this.stream.write(data)
	}

	async openStream() {
		const filepath = this.makeFilePath()
		const dir = path.dirname(filepath)
		if (fs.existsSync(dir)) {
			const stat = await fs.promises.stat(dir)
			if (!stat.isDirectory) {
				throw new Error(`Cannot create parent directory: "${dir}" exists but it is not a directory")`)
			}
		} else {
			await fs.promises.mkdir(dir, { recursive: true })
		}
		if (this.stream) {
			this.stream.end()
		}
		this.stream = fs.createWriteStream(filepath, { flags: "a" })
	}

	makeFilePath() {
		const date = new Date()
		const table = {
			yyyy: String(date.getFullYear()),
			yy: String(date.getFullYear()).slice(-2),
			mm: String(date.getMonth() + 1).padStart(2, "0"),
			m: String(date.getMonth() + 1),
			dd: String(date.getDate()).padStart(2, "0"),
			d: String(date.getDate()),
		}
		const name = this.filename_format
			.map((e, i) => {
				if (i % 2) {
					return table[e.toLowerCase()]
				} else {
					return e
				}
			})
			.join("")
		return path.join(this.dir, name)
	}
}
