const { create } = require("../promise.js")

class BufferedStreamReader {
	buf = Buffer.alloc(0)
	promise = create()
	closed = false

	write = buf => {
		if (this.closed) return
		this.buf = Buffer.concat([this.buf, buf])
	}

	end = () => {
		if (this.closed) return
		this.closed = true
		this.promise.resolve(this.buf)
	}

	read = as_str => {
		if (as_str) {
			return this.promise.then(x => x.toString(as_str))
		} else {
			return this.promise
		}
	}

	pipeFrom(read_stream) {
		read_stream.on("data", this.write)
		read_stream.on("end", this.end)
	}
}

module.exports = BufferedStreamReader
