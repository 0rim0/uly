const default_levels = ["debug", "info", "warn", "error"]

const console_writer = (level, values) => {
	if (console[level]) {
		console[level](new Date(), ...values)
	} else {
		console.info(new Date(), ...values)
	}
}

const logger = (level, levels, writers) => async (...values) =>
	Promise.allSettled(
		writers.map(writer => {
			if (!writer) return
			if (writer === "console") writer = console_writer
			if (typeof writer === "function") return writer(level, values)
			if (writer.min_level && levels.indexOf(writer.min_level) <= levels.indexOf(level)) {
				if (writer.write) return writer.write(level, values)
				if (writer.writer && writer.writer.write) return writer.writer.write(level, values)
			}
		})
	)

module.exports = (writers, levels) => {
	levels = levels || default_levels
	return Object.fromEntries(levels.map(level => [level, logger(level, levels, writers)]))
}
