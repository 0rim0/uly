module.exports = {
	escape(text) {
		const map = { "&": "&amp;", '"': "&quot;", "<": "&lt;", ">": "&gt;" }
		return text.replace(/["&<>]/g, e => map[e])
	},

	unescape(html) {
		const map = { "&amp;": "&", "&quot;": '"', "&lt;": "<", "&gt;": ">" }
		return html.replace(/&amp;|&quot;|&lt;|&gt;/g, e => map[e])
	},
}
