module.exports = {
	copy(value) {
		if (value instanceof Object) {
			return JSON.parse(JSON.stringify(value))
		}
		return value
	},

	pick(obj, keys) {
		const result = {}
		for (const k of keys) {
			if (k in obj) result[k] = obj[k]
		}
		return result
	},

	withhold(obj, keys) {
		const result = {}
		const key_set = new Set(keys)
		for (const [k, v] of Object.entries(obj)) {
			if (!key_set.has(k)) result[k] = v
		}
		return result
	},

	filter(obj, fn) {
		const result = {}
		for (const [k, v] of Object.entries(obj)) {
			if (fn(v, k, obj)) result[k] = v
		}
		return result
	},

	map(obj, fn) {
		const result = {}
		for (const [k, v] of Object.entries(obj)) {
			result[k] = fn(v, k, obj)
		}
		return result
	},

	flatMap(obj, fn) {
		const result = {}
		for (const [k, v] of Object.entries(obj)) {
			Object.assign(result, fn ? fn(v, k, obj) : v)
		}
		return result
	},

	diff(from, to, compare) {
		const added = new Set(Object.keys(to))
		const removed = new Set()
		const unchanged = new Set()
		const changed = new Set()
		for (const key of Object.keys(from)) {
			if (added.has(key)) {
				added.delete(key)
				const equal = typeof compare === "function" ? compare(from[key], to[key]) : from[key] === to[key]
				if (equal) {
					unchanged.add(key)
				} else {
					changed.add(key)
				}
			} else {
				removed.add(key)
			}
		}
		return { added, removed, unchanged, changed }
	},
}
